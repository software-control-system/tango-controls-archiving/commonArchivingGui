package fr.soleil.archiving.gui.logs;

public enum LogLevel {
	TRACE, DEBUG, INFO, WARN, ERROR;
}
