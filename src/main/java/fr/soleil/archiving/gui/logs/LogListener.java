package fr.soleil.archiving.gui.logs;

import java.util.EventListener;

public interface LogListener extends EventListener {

	public void logChanged();

}
