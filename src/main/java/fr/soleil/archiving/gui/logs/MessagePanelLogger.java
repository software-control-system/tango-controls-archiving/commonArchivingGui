/**
 * 
 */
package fr.soleil.archiving.gui.logs;

import org.slf4j.ILoggerFactory;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import fr.soleil.archiving.gui.messages.MessagesPanel;

/**
 * @author guest
 * @deprecated You should use {@link fr.soleil.lib.project.application.logging.LogManager} instead
 */
@Deprecated
public class MessagePanelLogger extends AppenderBase<ILoggingEvent> {

    public static final String DEFAULT_PATTERN_LAYOUT = "%d{dd-MM-yy HH:mm:ss.SSS} - %-5p: %m%n";

    private PatternLayout layout;

    /**
     * 
     */
    public MessagePanelLogger() {
        LoggerContext loggerContext = new LoggerContext();

        final ILoggerFactory loggerFactory = LoggerFactory.getILoggerFactory();
        if (loggerFactory instanceof LoggerContext) {
            loggerContext = (LoggerContext) loggerFactory;
        }

        layout = new PatternLayout();
        layout.setPattern(DEFAULT_PATTERN_LAYOUT);
        layout.setContext(loggerContext);
        layout.start();
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        final MessagesPanel messagesPanel = MessagesPanel.getInstance();
        final String[] lines = layout.doLayout(eventObject).split("\n");
        for (final String line : lines) {
            messagesPanel.getMessagesArea().addTrace(line);
        }
    }
}
