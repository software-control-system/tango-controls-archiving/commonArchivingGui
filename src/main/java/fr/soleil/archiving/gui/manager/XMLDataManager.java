package fr.soleil.archiving.gui.manager;

import java.io.File;
import java.io.IOException;

import org.w3c.dom.Node;

import fr.soleil.archiving.common.api.exception.ArchivingException;
import fr.soleil.lib.project.xmlhelpers.XMLUtils;

/**
 * A class able to save/load data from/to xml files
 *
 * @param <D> The type of data manipulated by this {@link XMLDataManager}
 * @param <M> The type of data loaded in {@link #loadDataIntoHash(String)} and {@link #loadDataIntoHashFromRoot(Node)}
 * 
 * @author thiamm
 */
public abstract class XMLDataManager<D, M> {

    /**
     * Returns the default data to use if the xml file in {@link #loadDataIntoHash(String)} is empty
     * 
     * @return Some data. Never <code>null</code>.
     */
    protected abstract D getDefaultData();

    /**
     * Saves data to the desired location.
     * 
     * @param data The data to save
     * @param resourceLocation The save location
     * @throws ArchivingException If a problem occurred
     */
    protected final void saveData(D data, String resourceLocation) throws ArchivingException {
        if (data == null) {
            throw new ArchivingException("Can't save null data in " + resourceLocation);
        } else {
            try {
                XMLUtils.save(data.toString(), resourceLocation);
            } catch (IOException e) {
                throw new ArchivingException("Failed to save " + data.getClass().getSimpleName() + " in "
                        + resourceLocation);
            }
        }
    }

    /**
     * Loads the data into <code>M</code>, given the root node of the XML file.
     * 
     * @param rootNode The DOM root of the XML file
     * @return The loaded data
     * @throws ArchivingException If a problem occurred
     */
    protected abstract M loadDataIntoHashFromRoot(Node rootNode) throws ArchivingException;

    protected boolean canSaveDefaultDataInLoadData() {
        return true;
    }

    /**
     * Loads the data into a Map, given the path of the XML file.
     * 
     * @param resourceLocation The path of the XML file
     * @return The loaded data
     * @throws ArchivingException If a problem occurred
     */
    protected M loadDataIntoHash(String resourceLocation) throws ArchivingException {
        M result;
        boolean catchException = true;
        try {
            File file = new File(resourceLocation);
            if (file.exists()) {
                Node rootNode = XMLUtils.getRootNode(file);
                result = loadDataIntoHashFromRoot(rootNode);
            } else {
                File parent = file.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
                if (canSaveDefaultDataInLoadData()) {
                    saveData(getDefaultData(), resourceLocation);
                    Node rootNode = XMLUtils.getRootNode(file);
                    result = loadDataIntoHashFromRoot(rootNode);
                } else {
                    catchException = false;
                    throw new ArchivingException(file.getPath() + " does not exist!");
                }
            }
        } catch (Exception e) {
            if ((!catchException) && (e instanceof ArchivingException)) {
                throw ((ArchivingException) e);
            } else {
                throw XMLManager.generateFileArchivingException(resourceLocation);
            }
        }
        return result;
    }

}
