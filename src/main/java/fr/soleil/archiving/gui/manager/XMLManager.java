package fr.soleil.archiving.gui.manager;

import java.io.File;

import fr.soleil.archiving.common.api.exception.ArchivingException;

public class XMLManager {
    public static File getFile(String path) throws ArchivingException {
        File file;
        try {
            file = new File(path);
            if (!file.exists()) {
                // First time: ensure parent path exists
                File parent = file.getParentFile();
                if (!parent.exists()) {
                    parent.mkdirs();
                }
            }
        } catch (Exception e) {
            throw generateFileArchivingException(path);
        }
        return file;
    }

    public static ArchivingException generateFileArchivingException(String path) {
        return new ArchivingException(path + " can't be accessed or is corrupted!");
    }

}
