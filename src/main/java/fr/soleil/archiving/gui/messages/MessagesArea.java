//+======================================================================
// $Source$
//
// Project:      Tango Archiving Service
//
// Description:  Java source code for the class  MessagesArea.
//						(Claisse Laurent) - 13 juin 2005
//
// $Author$
//
// $Revision$
//
// $Log$
// Revision 1.5  2005/11/29 18:25:27  chinkumo
// no message
//
// Revision 1.1.1.2  2005/08/22 11:58:35  chinkumo
// First commit
//
//
// copyleft :		Synchrotron SOLEIL
//					L'Orme des Merisiers
//					Saint-Aubin - BP 48
//					91192 GIF-sur-YVETTE CEDEX
//
//-======================================================================
package fr.soleil.archiving.gui.messages;

import java.awt.Color;
import java.awt.Component;
import java.util.Vector;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;

import org.jdesktop.swingx.JXTable;
import org.jdesktop.swingx.decorator.ColorHighlighter;
import org.jdesktop.swingx.decorator.HighlightPredicate;
import org.jdesktop.swingx.decorator.SearchPredicate;

import fr.soleil.archiving.gui.logs.LogLevel;
import fr.soleil.archiving.gui.logs.LogListener;

/**
 * A JTextArea component with preset rows and columns
 * 
 * @author CLAISSE
 */
public class MessagesArea extends JXTable {

    private static final long serialVersionUID = 4265639603217522815L;

    private Pattern patternLogFilter;
    private final EventListenerList listenerList = new EventListenerList();
    private int logMaxDepth = 3000;

    /**
     * Default constructor, sets up rows and columns
     */
    public MessagesArea() {
        super(new DefaultTableModel(new String[] { "logs" }, 0));
        setEditable(false);
        setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(final TableModelEvent e) {
                if (e.getType() == TableModelEvent.INSERT) {
                    SwingUtilities.invokeLater(new Runnable() {
                        @Override
                        public void run() {
                            scrollRectToVisible(getCellRect(e.getLastRow(), 0, false));
                        }
                    });

                }
            }
        });

        // add a default highligther for errors
        // new Color(1f, 0.2f, 0.2f, 1f)
        final ColorHighlighter errorHighlight = new ColorHighlighter(HighlightPredicate.ALWAYS, Color.RED, null);
        final HighlightPredicate error = new SearchPredicate(Pattern.compile("ERROR"));
        errorHighlight.setHighlightPredicate(error);

        final ColorHighlighter warnHighlight = new ColorHighlighter(HighlightPredicate.ALWAYS, Color.ORANGE, null);
        final HighlightPredicate warn = new SearchPredicate(Pattern.compile("WARN"));
        warnHighlight.setHighlightPredicate(warn);

        addHighlighter(warnHighlight);
        addHighlighter(errorHighlight);
    }

    public void addTrace(final String trace) {
        // clear old traces
        DefaultTableModel model = (DefaultTableModel) getModel();
        int currentSize = model.getRowCount();
        if (currentSize > logMaxDepth) {
            clearTraces();
        }
        // trace
        if (patternLogFilter != null) {
            final Matcher matcher = patternLogFilter.matcher(trace);
            if (matcher.find()) {
                ((DefaultTableModel) getModel()).addRow(new Object[] { trace });
                fireLogChanged();
            }
        } else {
            ((DefaultTableModel) getModel()).addRow(new Object[] { trace });
            fireLogChanged();
        }
    }

    public void clearTraces() {
        Vector<String> vector = new Vector<String>();
        vector.add("logs");
        ((DefaultTableModel) getModel()).setDataVector(new Vector<String>(), vector);
    }

    @Override
    public Component prepareRenderer(final TableCellRenderer renderer, final int rowIndex, final int vColIndex) {
        final Component c = super.prepareRenderer(renderer, rowIndex, vColIndex);
        if (c instanceof JComponent) {
            final JComponent jc = (JComponent) c;
            jc.setToolTipText((String) getValueAt(rowIndex, vColIndex));
        }
        return c;
    }

    public void setLogLevel(final LogLevel logLevel) {
        String criterious;
        switch (logLevel) {
            case TRACE:
                criterious = "TRACE|DEBUG|INFO|WARN|ERROR";
                break;
            case DEBUG:
                criterious = "DEBUG|INFO|WARN|ERROR";
                break;
            case INFO:
                criterious = "INFO|WARN|ERROR";
                break;
            case WARN:
                criterious = "WARN|ERROR";
                break;
            case ERROR:
            default:
                criterious = "ERROR";
                break;
        }

        patternLogFilter = Pattern.compile(criterious, Pattern.CASE_INSENSITIVE);
    }

    public void setMaxLogDepth(int logMaxDepth) {
        this.logMaxDepth = logMaxDepth;
    }

    public void addLogListener(final LogListener listener) {
        listenerList.add(LogListener.class, listener);
    }

    public void removeLogListener(final LogListener listener) {
        listenerList.remove(LogListener.class, listener);
    }

    private void fireLogChanged() {
        for (final LogListener listener : listenerList.getListeners(LogListener.class)) {
            listener.logChanged();
        }
    }

}
