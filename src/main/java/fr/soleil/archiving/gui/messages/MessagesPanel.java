// +======================================================================
// $Source:
// /cvsroot/tango-cs/tango/tools/bensikin/bensikin/containers/messages/MessagesPanel.java,v
// $
//
// Project: Tango Archiving Service
//
// Description: Java source code for the class MessagesPanel.
// (Claisse Laurent) - 16 juin 2005
//
// $Author$
//
// $Revision$
//
// $Log$
// Revision 1.5 2005/11/29 18:25:13 chinkumo
// no message
//
// Revision 1.1.1.2 2005/08/22 11:58:36 chinkumo
// First commit
//
//
// copyleft : Synchrotron SOLEIL
// L'Orme des Merisiers
// Saint-Aubin - BP 48
// 91192 GIF-sur-YVETTE CEDEX
//
// -======================================================================
package fr.soleil.archiving.gui.messages;

import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BorderFactory;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.gui.logs.LogLevel;
import fr.soleil.archiving.gui.logs.LogListener;
import fr.soleil.archiving.gui.logs.MessagePanelLogger;

/**
 * Panel containing the application's logs
 * 
 * @author CLAISSE
 * @deprecated You should use {@link fr.soleil.lib.project.application.logging.LogViewer} instead.
 */
@Deprecated
public class MessagesPanel extends JPanel {

    private static final long serialVersionUID = 5415080931326535195L;

    private final MessagesArea messagesArea;
    private static final MessagesPanel messagesPanelInstance = new MessagesPanel();

    final static Logger logger = LoggerFactory.getLogger(MessagesPanel.class);

    /**
     * Instantiates itself if necessary, returns the instance.
     * 
     * @return The instance
     */
    public static MessagesPanel getInstance() {
        return messagesPanelInstance;
    }

    /**
     * Builds the panel
     */
    private MessagesPanel() {
        setLayout(new BorderLayout());
        messagesArea = new MessagesArea();
        this.add(messagesArea, BorderLayout.CENTER);
        final TitledBorder tb = BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED),
                "logs", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP);
        setBorder(tb);

        /* SB -- Ajout JPopupMen dans la console de log */
        final JPopupMenu messagePopupMenu = new JPopupMenu();
        messagesArea.setComponentPopupMenu(messagePopupMenu);
        JMenuItem itemBox = new JMenuItem("Clear");
        messagePopupMenu.add(itemBox);
        itemBox.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logger.info("Clear logs");
                messagesArea.clearTraces();
            }

        });

        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    messagePopupMenu.show(e.getComponent(), e.getX(), e.getY());
                }
            }
        });
        /* SB -- End */
        // GUIUtilities.setObjectBackground(this, GUIUtilities.MESSAGE_COLOR);
    }

    /**
     * @return The messagesArea attribute.
     */
    public MessagesArea getMessagesArea() {
        return messagesArea;
    }

    public void setLogLevel(final LogLevel logLevel) {
        messagesArea.setLogLevel(logLevel);
    }

    public void addLogger(final String loggerName) {
        // Logger logger = LoggerFactory.getLogger("fr.soleil.bensikin");
        final Logger logger = LoggerFactory.getLogger(loggerName);
        if (logger != null && logger instanceof ch.qos.logback.classic.Logger) {
            ((ch.qos.logback.classic.Logger) logger).addAppender(new MessagePanelLogger());
        }
    }

    public void addLogListener(final LogListener listener) {
        messagesArea.addLogListener(listener);
    }

    public void removeLogListener(final LogListener listener) {
        messagesArea.removeLogListener(listener);
    }
}
