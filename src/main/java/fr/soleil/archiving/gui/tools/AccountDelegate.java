package fr.soleil.archiving.gui.tools;

import java.awt.GraphicsEnvironment;
import java.io.File;

import javax.swing.JOptionPane;

import org.slf4j.Logger;

import fr.soleil.lib.project.application.user.manager.AccountManager;
import fr.soleil.lib.project.swing.Splash;

/**
 * A class delegated to account selection
 * 
 * @author thiamm
 */
public class AccountDelegate {

    protected String selectedAccountPath;

    public AccountDelegate() {
        this.selectedAccountPath = null;
    }

    /**
     * Launches account selector, returning whether account selection was canceled.
     * To get the selected account path, use {@link #getSelectedAccountPath()}.
     * <p>
     * Calling this method will automatically create the corresponding directory if it does not exist, exiting the JVM
     * when directory creation failed.
     * <p>
     * 
     * @param applicationName The application name (for error popups)
     * @param accountManager The {@link AccountManager} that manages accounts
     * @param splash The {@link Splash} above which to show error popups
     * @param logger The {@link Logger} in which to trace messages
     * @return A <code>boolean</code>. <code>true</code> if account selection was canceled, <code>false</code>
     *         otherwise.
     */
    public boolean launchAccountSelector(final String applicationName, final AccountManager accountManager,
            final Splash splash, final Logger logger) {
        boolean cancel;
        if (accountManager == null) {
            cancel = true;
        } else {
            accountManager.launchAccountSelector();
            if (accountManager.getSelectedAccount() == AccountManager.ACCOUNT_SELECTION_CANCELED) {
                cancel = true;
            } else if (accountManager.getSelectedAccount() == AccountManager.NO_ACCOUNT_SELECTED) {
                // relaunch account selector
                cancel = launchAccountSelector(applicationName, accountManager, splash, logger);
            } else {
                cancel = false;
                selectedAccountPath = accountManager.getSelectedAccountWorkingPath();
                boolean directoryOk = true;
                try {
                    if (selectedAccountPath == null) {
                        directoryOk = false;
                    } else {
                        File tmp = new File(selectedAccountPath);
                        if (tmp.exists()) {
                            directoryOk = tmp.canWrite();
                        } else {
                            directoryOk = tmp.mkdirs();
                        }
                    }
                } catch (Exception e) {
                    if (logger != null) {
                        logger.error(selectedAccountPath + " directory creation failed", e);
                    }
                    directoryOk = false;
                }
                if (directoryOk) {
                    if (logger != null) {
                        logger.debug("account is {} with path {} ", accountManager.getSelectedAccountName(),
                                selectedAccountPath);
                    }
                    Runtime.getRuntime()
                            .addShutdownHook(new Thread("Clear AccountManager lock at " + selectedAccountPath) {
                                @Override
                                public void run() {
                                    accountManager.clearLock(selectedAccountPath);
                                }
                            });
                } else {
                    if (!GraphicsEnvironment.isHeadless()) {
                        JOptionPane.showMessageDialog(splash,
                                applicationName + " can't write directory " + selectedAccountPath, "Error!",
                                JOptionPane.ERROR_MESSAGE);
                    }
                    System.exit(1);
                }
            } // end if (accountManager.getSelectedAccount() == AccountManager.NO_ACCOUNT_SELECTED) ... else
        } // end if (accountManager == null) ... else
        return cancel;
    }

    /**
     * Returns the selected account path, when account selection was called and not canceled.
     * 
     * @return A {@link String}
     * @see #launchAccountSelector(AccountManager, Splash, Logger)
     */
    public String getSelectedAccountPath() {
        return selectedAccountPath;
    }
}
