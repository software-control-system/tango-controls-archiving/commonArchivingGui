package fr.soleil.archiving.tango;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tango.utils.DevFailedUtils;

import fr.esrf.Tango.DevFailed;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.DeviceProxy;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Family;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.lib.project.ICancelable;
import fr.soleil.lib.project.date.DateUtil;

public class TangoLoaderUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(TangoLoaderUtil.class);

    private static final String TANGO_SEPARATOR = GUIUtilities.TANGO_DELIM;
    private static final String TANGO_JOKER = "*";
    private static final String REGEX_JOKER = ".*";
    private static final String TANGO_ANY = "?";
    private static final String REGEX_ANY = ".";
    private static final String NADA = "nada";

    /**
     * Search for devices and attributes in tango db with a search pattern, with the possibility to cancel search
     * through an {@link ICancelable}.
     *
     * @param searchPattern The search pattern. Wild char is *.
     * @param cancelable The {@link ICancelable} that indicates whether search should be cancelled. Can be
     *            <code>null</code>.
     * @return A {@link Domain} {@link List}, never <code>null</code>.
     */
    public static List<Domain> searchAttributes(final String searchPattern, ICancelable cancelable) {
        final long t1 = System.currentTimeMillis();
        final List<Domain> ret = new ArrayList<>();
        if (searchPattern != null) {
            // Use lower case to avoid case sensitivity (TANGOARCH-493)
            final String pattern = searchPattern.toLowerCase();
            String devicePattern = pattern;
            String attributePattern = TANGO_JOKER;
            if (pattern.contains(TANGO_SEPARATOR)) {
                if (StringUtils.countMatches(devicePattern, TANGO_SEPARATOR) == 3) {
                    devicePattern = pattern.substring(0, pattern.lastIndexOf('/'));
                    attributePattern = pattern.substring(pattern.lastIndexOf('/') + 1);
                }
                if (!attributePattern.equals(TANGO_JOKER)) {
                    LOGGER.warn("looking for attributes {} can be very slow", attributePattern);
                }
            }
            LOGGER.debug("looking for devices with pattern: device name = {}, attribute name = {}", devicePattern,
                    attributePattern);
            try {
                final String[] deviceNames = ApiUtil.get_db_obj().get_device_exported(devicePattern);
                LOGGER.debug("found {} devices for pattern {}", deviceNames.length, devicePattern);
                int attributesFound = 0;
                // debug booleans for PROBLEM-1823
                boolean nadaDevice = false, nullDevice = false, emptyDevice = false;
                for (final String deviceName : deviceNames) {
                    // PROBLEM-1823: avoid undesired device names
                    if (deviceName == null) {
                        nullDevice = true;
                        continue;
                    } else if (NADA.equalsIgnoreCase(deviceName)) {
                        nadaDevice = true;
                        continue;
                    } else if (deviceName.trim().isEmpty()) {
                        emptyDevice = true;
                        continue;
                    }
                    if ((cancelable != null) && (cancelable.isCanceled())) {
                        break;
                    }
                    final String[] split = deviceName.split(TANGO_SEPARATOR);
                    if (split.length == 3) {
                        final String domainName = split[0];
                        final String familyName = split[1];
                        final String memberName = split[2];
                        String[] attributeNames = new String[0];
                        if (attributePattern.equals(TANGO_JOKER)) {
                            // add only device name to result
                            int i = 0;
                            for (final Domain domain : ret) {
                                if ((cancelable != null) && (cancelable.isCanceled())) {
                                    break;
                                }
                                if (domain.getName().equalsIgnoreCase(domainName)) {
                                    Family family = domain.getFamily(familyName);
                                    Member member;
                                    if (family == null) {
                                        family = new Family(familyName);
                                        member = new Member(memberName);
                                        family.addMember(member);
                                        domain.addFamily(family);
                                        break;
                                    } else {
                                        member = family.getMember(memberName);
                                        if (member == null) {
                                            member = new Member(memberName);
                                            family.addMember(member);
                                            break;
                                        } else {
                                            break;
                                        }
                                    }
                                }
                                i++;
                            }
                            if (i == ret.size()) {
                                final Domain d = new Domain(domainName);
                                final Family f = new Family(familyName);
                                final Member m = new Member(memberName);
                                f.addMember(m);
                                d.addFamily(f);
                                ret.add(d);
                            }
                        } else {
                            if ((cancelable != null) && (cancelable.isCanceled())) {
                                break;
                            }
                            // search for attributes
                            try {
                                final DeviceProxy deviceProxy = new DeviceProxy(deviceName);
                                // Following line is where "nada" was detected in PROBLEM-1823, so deviceName was "nada"
                                attributeNames = deviceProxy.get_attribute_list();
                            } catch (final DevFailed e) {
                            }
                            String attrPattern = attributePattern.replace(TANGO_JOKER, REGEX_JOKER).replace(TANGO_ANY,
                                    REGEX_ANY);
                            for (final String currentAttributeName : attributeNames) {
                                if ((cancelable != null) && (cancelable.isCanceled())) {
                                    break;
                                }
                                if (currentAttributeName != null) {
                                    // Use lower case to avoid case sensitivity (TANGOARCH-493)
                                    if (Pattern.matches(attrPattern, currentAttributeName.toLowerCase())) {
                                        attributesFound++;
                                        final Attribute currenteAttribute = new Attribute(currentAttributeName);
                                        int i = 0;
                                        for (final Domain domain : ret) {
                                            if ((cancelable != null) && (cancelable.isCanceled())) {
                                                break;
                                            }
                                            if (domain.getName().equalsIgnoreCase(domainName)) {
                                                Family family = domain.getFamily(familyName);
                                                Member member;
                                                if (family == null) {
                                                    family = new Family(familyName);
                                                    member = new Member(memberName);
                                                    member.addAttribute(currenteAttribute);
                                                    family.addMember(member);
                                                    domain.addFamily(family);
                                                    break;
                                                } else {
                                                    member = family.getMember(memberName);
                                                    if (member == null) {
                                                        member = new Member(memberName);
                                                        member.addAttribute(currenteAttribute);
                                                        family.addMember(member);
                                                        break;
                                                    } else {
                                                        member.addAttribute(currenteAttribute);
                                                        break;
                                                    }
                                                }
                                            }
                                            i++;
                                        }
                                        if ((cancelable != null) && (cancelable.isCanceled())) {
                                            break;
                                        }
                                        if (i == ret.size()) {
                                            final Domain d = new Domain(domainName);
                                            final Family f = new Family(familyName);
                                            final Member m = new Member(memberName);
                                            m.addAttribute(currenteAttribute);
                                            f.addMember(m);
                                            d.addFamily(f);
                                            ret.add(d);
                                        }
                                    }
                                } // end if (currentAttributeName != null)
                            } // end for (final String currentAttributeName : attributeNames)
                        } // end if (attributePattern.equals(TANGO_JOKER)) ... else
                    } // end if (split.length == 3)
                    if (!attributePattern.equals(GUIUtilities.TANGO_JOKER)) {
                        LOGGER.debug("found {} attribute(s) on {} device(s)", attributesFound, deviceNames.length);
                    }
                } // end for (final String deviceName : deviceNames)

                // debug for PROBLEM-1823
                if (nullDevice) {
                    LOGGER.debug("ApiUtil.get_db_obj().get_device_exported(\"" + devicePattern
                            + "\") returned some null values");
                }
                if (nadaDevice) {
                    LOGGER.debug("ApiUtil.get_db_obj().get_device_exported(\"" + devicePattern
                            + "\") returned some \"nada\"");
                }
                if (emptyDevice) {
                    LOGGER.debug("ApiUtil.get_db_obj().get_device_exported(\"" + devicePattern
                            + "\") returned some empty values");
                }
            } catch (final DevFailed e) {
                LOGGER.error("tango error {}", DevFailedUtils.toString(e));
            }
        } // end if (searchPattern != null)
        final long t2 = System.currentTimeMillis();
        LOGGER.debug(
                DateUtil.elapsedTimeToStringBuilder(new StringBuilder("search attributes ellapsed time: "), t2 - t1)
                        .toString());
        return ret;
    }

    /**
     * Search for devices and attributes in tango db with a search pattern
     *
     * @param searchPattern The search pattern. Wild char is *.
     * @return A {@link Domain} {@link List}, never <code>null</code>.
     */
    public static List<Domain> searchAttributes(final String searchPattern) {
        return searchAttributes(searchPattern, null);
    }

}
