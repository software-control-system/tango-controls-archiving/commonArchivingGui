package fr.soleil.archiving.tango.entity;

import java.lang.ref.WeakReference;
import java.util.StringTokenizer;

import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;

public class Attribute extends Entity {

    private static final long serialVersionUID = 7774130613161148998L;

    private String completeName;
    private String domainName;
    private String familyName;
    private String memberName;
    private String deviceName;

    private Attributes attributes;
    private WeakReference<Member> memberRef;

    private String deviceClass;
    private boolean isSelected = true;
    private boolean isNew = false;

    public Attribute() {
        super();
    }

    public Attribute(Attributes _contextAttributes) {
        super();
        this.attributes = _contextAttributes;
    }

    public Attribute(String name) {
        super(name);
    }

    public Attribute(TreeNode[] path) {
        super();
        String domain = path[1].toString();
        String family = path[2].toString();
        String member = path[3].toString();
        String name = path[4].toString();

        this.domainName = domain;
        this.familyName = family;
        this.memberName = member;
        this.deviceName = member;
        this.name = name;
        this.completeName = domain + GUIUtilities.TANGO_DELIM + family + GUIUtilities.TANGO_DELIM + member
                + GUIUtilities.TANGO_DELIM + name;
    }

    public TreePath getTreePath(String rootName) {
        TreePath ret = null;
        try {
            StringTokenizer st = new StringTokenizer(this.completeName, GUIUtilities.TANGO_DELIM);
            String[] path = new String[5];

            String domain = st.nextToken();
            String family = st.nextToken();
            String member = st.nextToken();
            String name = st.nextToken();

            path[0] = rootName;
            path[1] = domain;
            path[2] = family;
            path[3] = member;
            path[4] = name;

            ret = new TreePath(path);
        } catch (Exception e) {
            e.printStackTrace();
            ret = null;
        }
        return ret;
    }

    public Attributes getAttributes() {
        return attributes;
    }

    public void setAttributes(Attributes contextAttribute) {
        this.attributes = contextAttribute;
    }

    public String getCompleteName() {
        return completeName;
    }

    public void setCompleteName(String completeName) {
        this.completeName = completeName;

        if (completeName != null) {
            StringTokenizer st = new StringTokenizer(completeName, GUIUtilities.TANGO_DELIM);
            String domain_s = st.nextToken();
            String family_s = st.nextToken();
            String member_s = st.nextToken();
            String attr_s = st.nextToken();

            this.setDomain(domain_s);
            this.setFamily(family_s);
            this.setMember(member_s);
            this.setDevice(domain_s + GUIUtilities.TANGO_DELIM + family_s + GUIUtilities.TANGO_DELIM + member_s);
            this.setName(attr_s);
        }

    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDevice(String device) {
        this.deviceName = device;
    }

    public String getDomainName() {
        return domainName;
    }

    public void setDomain(String domain) {
        this.domainName = domain;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamily(String family) {
        this.familyName = family;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMember(String member) {
        this.memberName = member;
    }

    public String getDescription() {
        return "                " + this.getName();
    }

    public Member getMember() {
        return ObjectUtils.recoverObject(memberRef);
    }

    public void setMember(Member member) {
        this.memberRef = member == null ? null : new WeakReference<>(member);
    }

    public boolean isNew() {
        return isNew;
    }

    public void setNew(boolean isNew) {
        this.isNew = isNew;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean isSelected) {
        this.isSelected = isSelected;
    }

    public void reverseSelection() {
        this.isSelected = !this.isSelected;
    }

    public String getDeviceClass() {
        return deviceClass;
    }

    public void setDeviceClass(String deviceClass) {
        this.deviceClass = deviceClass;
    }

    public String[] getPath() {
        String[] pathRef = new String[5];
        pathRef[0] = ObjectUtils.EMPTY_STRING;
        pathRef[1] = this.getDomainName();
        pathRef[2] = this.getFamilyName();
        pathRef[3] = this.getMemberName();
        pathRef[4] = this.getName();
        return pathRef;
    }

    @Override
    public String toString() {
        return BIG_SPACE + this.getName();
    }

}
