package fr.soleil.archiving.tango.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class Attributes implements java.io.Serializable {

    private static final long serialVersionUID = -4720041423182692467L;

    private Attribute[] attributes;

    public Attributes() {
        super();
    }

    public List<Domain> getHierarchy() {
        ArrayList<Domain> result;
        if (this.attributes == null) {
            result = null;
        } else {

            int nbAttributes = this.attributes.length;

            // Map domains
            Map<String, Map<String, Map<String, List<String>>>> htDomains = new HashMap<String, Map<String, Map<String, List<String>>>>();
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.attributes[i];
                String currentDomain = currentAttribute.getDomainName();
                htDomains.put(currentDomain, new HashMap<String, Map<String, List<String>>>());
            }

            // Map families
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.attributes[i];
                String currentDomain = currentAttribute.getDomainName();
                String currentFamily = currentAttribute.getFamilyName();

                Map<String, Map<String, List<String>>> htCurrentDomain = htDomains.get(currentDomain);
                htCurrentDomain.put(currentFamily, new HashMap<String, List<String>>());
            }

            // Map members
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.attributes[i];
                String currentDomain = currentAttribute.getDomainName();
                String currentFamily = currentAttribute.getFamilyName();
                String currentMember = currentAttribute.getMemberName();

                Map<String, Map<String, List<String>>> htCurrentDomain = htDomains.get(currentDomain);
                Map<String, List<String>> htCurrentFamily = htCurrentDomain.get(currentFamily);

                htCurrentFamily.put(currentMember, new ArrayList<String>());
            }

            // List attributes
            for (int i = 0; i < nbAttributes; i++) {
                Attribute currentAttribute = this.attributes[i];
                String currentDomain = currentAttribute.getDomainName();
                String currentFamily = currentAttribute.getFamilyName();
                String currentMember = currentAttribute.getMemberName();
                String currentAttributeName = currentAttribute.getName();

                Map<String, Map<String, List<String>>> htCurrentDomain = htDomains.get(currentDomain);
                Map<String, List<String>> htCurrentFamily = htCurrentDomain.get(currentFamily);
                List<String> veCurrentMember = htCurrentFamily.get(currentMember);

                veCurrentMember.add(currentAttributeName);
            }

            result = new ArrayList<Domain>();

            for (Entry<String, Map<String, Map<String, List<String>>>> domainEntry : htDomains.entrySet()) {
                String nextDomainName = domainEntry.getKey();
                Domain nextDomain = new Domain(nextDomainName);
                result.add(nextDomain);

                Map<String, Map<String, List<String>>> htFamilies = domainEntry.getValue();
                for (Entry<String, Map<String, List<String>>> familyEntry : htFamilies.entrySet()) {
                    String nextFamilyName = familyEntry.getKey();
                    Family nextFamily = new Family(nextFamilyName);
                    nextDomain.addFamily(nextFamily);

                    Map<String, List<String>> htMembers = familyEntry.getValue();
                    for (Entry<String, List<String>> memberEntry : htMembers.entrySet()) {
                        String nextMemberName = memberEntry.getKey();
                        Member nextMember = new Member(nextMemberName);
                        nextFamily.addMember(nextMember);

                        List<String> vectAttr = memberEntry.getValue();
                        for (int i = 0; i < vectAttr.size(); i++) {
                            nextMember.addAttribute(new Attribute(vectAttr.get(i)));
                        }
                    }
                }
            }

            result.trimToSize();
        }
        return result;
    }

    public Attributes(Attribute[] _attributes) {
        this.attributes = _attributes;
    }

    public Attribute[] getAttributes() {
        return attributes;
    }

    public void setContextAttributes(Attribute[] attributes) {
        this.attributes = attributes;
    }
}
