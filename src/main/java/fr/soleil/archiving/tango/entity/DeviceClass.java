package fr.soleil.archiving.tango.entity;

import java.util.ArrayList;
import java.util.Collection;

public class DeviceClass extends Entity {

    private static final long serialVersionUID = -8458853366371898095L;

    private Collection<Member> devices;
    private Domain domain;

    public DeviceClass(String name) {
        super(name);
        this.devices = new ArrayList<Member>();
    }

    public void addDevice(Member member) {
        if (member != null) {
            ArrayList<Member> toRemove = new ArrayList<Member>();
            for (Member device : devices) {
                if ((device != null) && (device.getName() != null) && device.getName().equals(member.getName())) {
                    toRemove.add(device);
                }
            }
            devices.removeAll(toRemove);
            toRemove.clear();
            devices.add(member);
        }
    }

    /**
     * @return Returns the devices.
     */
    public Collection<Member> getDevices() {
        return devices;
    }

    /**
     * @param devices The devices to set.
     */
    public void setDevices(Collection<Member> devices) {
        this.devices = devices;
    }

    /**
     * @return Returns the domain.
     */
    public Domain getDomain() {
        return domain;
    }

    /**
     * @param domain
     *            The domain to set.
     */
    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public void addDevices(Collection<Member> membersToAdd) {
        if (membersToAdd != null) {
            for (Member member : membersToAdd) {
                addDevice(member);
            }
        }
    }
}
