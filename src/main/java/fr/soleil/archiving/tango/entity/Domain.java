package fr.soleil.archiving.tango.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.soleil.archiving.gui.tools.GUIUtilities;

public class Domain extends Entity {

    private static final long serialVersionUID = 6030099765148113032L;

    private final Map<String, Family> families;

    public Domain(final String name) {
        super(name);
        families = new TreeMap<>();
    }

    public void addFamily(final Family family) {
        synchronized (families) {
            family.setDomain(this);
            families.put(family.getName().toLowerCase(), family);
        }
    }

    public void removeFamily(final String familyName) {
        if (familyName != null) {
            synchronized (families) {
                families.remove(familyName.toLowerCase());
            }
        }
    }

    public Family getFamily(final String familyName) {
        Family family = null;
        if (familyName != null) {
            synchronized (families) {
                family = families.get(familyName.toLowerCase());
            }
        }
        return family;
    }

    public List<Family> getFamilies() {
        List<Family> familyList;
        synchronized (families) {
            familyList = new ArrayList<>(families.values());
        }
        return familyList;
    }

    public static List<Domain> addDomain(final List<Domain> in, final Domain domain) {
        if (in != null && domain != null) {
            List<Domain> toRemove = new ArrayList<>();
            for (int i = 0; i < in.size(); i++) {
                if (in.get(i) != null && domain.getName().equalsIgnoreCase(in.get(i).getName())) {
                    toRemove.add(in.get(i));
                }
            }
            in.removeAll(toRemove);
            toRemove.clear();
            toRemove = null;
            in.add(domain);
        }
        return in;
    }

    public Family[] getFamiliesAsArray() {
        final List<Family> familyList;
        synchronized (families) {
            familyList = new ArrayList<>(families.values());
        }
        return familyList.toArray(new Family[familyList.size()]);
    }

    public String[] getFamilyNames() {
        String[] names;
        synchronized (families) {
            final int count = families.size();
            names = families.keySet().toArray(new String[count]);
        }
        return names;
    }

    public static Domain hasDomain(final List<Domain> domainsList, final String domainName) {
        if (domainsList != null) {
            for (int i = 0; i < domainsList.size(); i++) {
                if (domainsList.get(i) != null && domainsList.get(i).getName() != null
                        && domainsList.get(i).getName().equalsIgnoreCase(domainName)) {
                    return domainsList.get(i);
                }
            }
        }
        return null;
    }

    public void addAttribute(final String familyName, final String memberName, final String attributeName) {
        Family family = getFamily(familyName);
        if (family == null) {
            family = new Family(familyName);
            addFamily(family);
        }

        family.addAttribute(memberName, attributeName);
    }

    public String getDescription() {
        final StringBuilder buff = new StringBuilder();
        buff.append(SMALL_SPACE).append(getName());
        buff.append(GUIUtilities.CRLF);
        synchronized (families) {
            for (Family nextFamily : families.values()) {
                buff.append(nextFamily.getDescription());
                buff.append(GUIUtilities.CRLF);
            }
        }
        return buff.toString();
    }

    @Override
    public String toString() {
        final StringBuilder buffer = new StringBuilder(getName());
        if (families != null) {
            synchronized (families) {
                for (Family nextFamily : families.values()) {
                    buffer.append(GUIUtilities.CRLF);
                    buffer.append(nextFamily);
                }
            }
        }
        return buffer.toString();
    }

}
