package fr.soleil.archiving.tango.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.soleil.archiving.gui.tools.GUIUtilities;

public class Domains implements java.io.Serializable {

    private static final long serialVersionUID = 36390595418732407L;

    private static final String DESC_START = "    VVVVVVVVVVVVV Domains VVVVVVVVVVVVVVVV";
    private static final String DESC_END = "    ^^^^^^^^^^^^^ Domains ^^^^^^^^^^^^^^^^";

    private final Map<String, Domain> domains;

    public Domains() {
        this.domains = new HashMap<>();
    }

    public void addAttribute(String domainName, String familyName, String memberName, String attributeName) {
        Domain domain = this.getDomain(domainName);
        if (domain == null) {
            domain = new Domain(domainName);
            this.addDomain(domain);
        }

        domain.addAttribute(familyName, memberName, attributeName);
    }

    public List<Domain> getList() {
        List<Domain> ret = new ArrayList<Domain>();
        ret.addAll(domains.values());
        return ret;
    }

    public Domain getDomain(String domainName) {
        return domains.get(domainName);
    }

    public void addDomain(Domain domain) {
        domains.put(domain.getName(), domain);
    }

    public String getDescription() {
        StringBuilder buff = new StringBuilder();
        buff.append(DESC_START);
        buff.append(GUIUtilities.CRLF);
        for (Domain nextDomain : domains.values()) {
            buff.append(nextDomain.getDescription());
            buff.append(GUIUtilities.CRLF);
        }
        buff.append(DESC_END);
        return buff.toString();
    }
}
