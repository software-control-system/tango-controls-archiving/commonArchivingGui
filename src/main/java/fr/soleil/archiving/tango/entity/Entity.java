package fr.soleil.archiving.tango.entity;

import java.io.Serializable;

import fr.soleil.lib.project.ObjectUtils;

public class Entity implements Serializable {

    private static final long serialVersionUID = -7507101233057909780L;

    protected static final String SMALL_SPACE = "    ";
    protected static final String SPACE = "        ";
    protected static final String BIG_SPACE = "            ";

    protected String name;

    public Entity() {
        this(null);
    }

    public Entity(String name) {
        super();
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? ObjectUtils.EMPTY_STRING : name;
    }

}
