package fr.soleil.archiving.tango.entity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;

public class Family extends Entity {

    private static final long serialVersionUID = 4793234551216807658L;

    private final Map<String, Member> members;
    private WeakReference<Domain> domainRef;

    /**
     * @param _name
     */
    public Family(String name) {
        super(name);
        members = new TreeMap<>();
    }

    /**
     * @param member
     *            8 juil. 2005
     */
    public void addMember(Member member) {
        member.setFamily(this);
        members.put(member.getName().toLowerCase(), member);
    }

    public void removeMember(String memberName) {
        if (memberName != null) {
            members.remove(memberName.toLowerCase());
        }
    }

    public Member getMember(String memberName) {
        return memberName == null ? null : members.get(memberName.toLowerCase());
    }

    public List<Member> getMembers() {
        return new ArrayList<>(members.values());
    }

    public void addAttribute(String memberName, String attributeName) {
        Member member = getMember(memberName);
        if (member == null) {
            member = new Member(memberName);
            addMember(member);
        }

        member.addAttribute(new Attribute(attributeName));
    }

    public String getDescription() {
        StringBuilder buff = new StringBuilder(BIG_SPACE);
        buff.append(getName());
        buff.append(GUIUtilities.CRLF);
        for (Member member : members.values()) {
            buff.append(member.getDescription());
            buff.append(GUIUtilities.CRLF);
        }
        return buff.toString();
    }

    /**
     * @return the domain
     */
    public Domain getDomain() {
        return ObjectUtils.recoverObject(domainRef);
    }

    /**
     * @param domain
     *            the domain to set
     */
    public void setDomain(Domain domain) {
        this.domainRef = domain == null ? null : new WeakReference<>(domain);
    }

    @Override
    public String toString() {
        StringBuilder buff = new StringBuilder(SPACE);
        buff.append(getName());

        if (members != null) {
            for (Member member : members.values()) {
                buff.append(GUIUtilities.CRLF);
                buff.append(member);
            }
        }

        return buff.toString();
    }

}
