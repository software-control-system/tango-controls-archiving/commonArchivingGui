package fr.soleil.archiving.tango.entity;

public interface IPreBufferingEventListener {

    public void allDone();

    public void stepDone(int step, int totalSteps);

    public String getName();

}
