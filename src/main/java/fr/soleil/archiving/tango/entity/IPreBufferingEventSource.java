package fr.soleil.archiving.tango.entity;

public interface IPreBufferingEventSource {

    public void register(IPreBufferingEventListener source);

    public void unregister(IPreBufferingEventListener source);

    public void notifyAllDone();

    public void notifyStepDone(int step, int totalSteps);

}
