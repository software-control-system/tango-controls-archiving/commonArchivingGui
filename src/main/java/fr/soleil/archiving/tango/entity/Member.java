package fr.soleil.archiving.tango.entity;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;

public class Member extends Entity {

    private static final long serialVersionUID = -6793270519124175099L;

    private final Map<String, Attribute> attributes;
    private WeakReference<Family> familyRef;

    public Member(String name) {
        super(name);
        attributes = new TreeMap<>();
    }

    public void addAttribute(Attribute attribute) {
        attribute.setMember(this);
        attributes.put(attribute.getName().toLowerCase(), attribute);
    }

    public List<Attribute> getAttributes() {
        return new ArrayList<>(attributes.values());
    }

    public Attribute getAttribute(String attrName) {
        return attrName == null ? null : attributes.get(attrName.toLowerCase());
    }

    public void removeAttribute(String attrToRemove) {
        if (attrToRemove != null) {
            attributes.remove(attrToRemove.toLowerCase());
        }
    }

    public String getDescription() {
        StringBuilder buff = new StringBuilder();
        buff.append(BIG_SPACE + getName());
        buff.append(GUIUtilities.CRLF);
        for (Attribute nextAttribute : attributes.values()) {
            buff.append(nextAttribute.getDescription());
            buff.append(GUIUtilities.CRLF);
        }
        return buff.toString();
    }

    public Family getFamily() {
        return ObjectUtils.recoverObject(familyRef);
    }

    public void setFamily(Family family) {
        this.familyRef = family == null ? null : new WeakReference<>(family);
    }

    @Override
    public String toString() {
        StringBuilder buffer = new StringBuilder(SPACE);
        buffer.append(getName());

        if (attributes != null) {
            for (Attribute nextAttribute : attributes.values()) {
                buffer.append(GUIUtilities.CRLF);
                buffer.append(nextAttribute);
            }
        }

        return buffer.toString();
    }

}
