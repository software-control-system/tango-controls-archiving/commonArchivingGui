package fr.soleil.archiving.tango.entity.bean;

import java.util.Locale;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.datasource.BasicTangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.datasource.BufferedTangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.datasource.DummyTangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.datasource.FilteredTangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.datasource.ITangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.datasource.SortedTangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.datasource.ThreadedBufferedTangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.ui.AttributesSelectionTablePanel;
import fr.soleil.lib.project.progress.Startable;
import fr.soleil.lib.project.resource.MessageManager;

public class AttributeTableSelectionBean implements Startable {

    private final static String PACKAGE_NAME = "fr.soleil.archiving.tango.entity";

    private AttributesSelectionTablePanel selectionPanel;
    private ITangoEntitiesSelectionManager tangoManager;
    private MessageManager messageManager;

    /**
     * Value used to create a useless {@link ITangoEntitiesSelectionManager} (an {@link ITangoEntitiesSelectionManager}
     * that does nothing).
     *
     * @see {@link #generateTangoManager(int)}
     */
    public static final int DUMMY = -1;

    /**
     * Value used to create a basic {@link ITangoEntitiesSelectionManager}.
     *
     * @see {@link #generateTangoManager(int)}
     */
    public static final int BASIC = 0;
    /**
     * Value used to create an {@link ITangoEntitiesSelectionManager} that
     * buffers its entities.
     *
     * @see {@link #generateTangoManager(int)}
     */
    public static final int BUFFERED = 1;
    /**
     * Value used to create an {@link ITangoEntitiesSelectionManager} that sorts
     * its entities.
     *
     * @see {@link #generateTangoManager(int)}
     */
    public static final int SORTED = 2;
    /**
     * Value used to create an {@link ITangoEntitiesSelectionManager} that
     * buffers and sorts its entities.
     *
     * @see {@link #generateTangoManager(int)}
     */
    public static final int BUFFERED_AND_SORTED = 3;

    /**
     * Default constructor
     */
    public AttributeTableSelectionBean() {
        this(null, null);
    }

    /**
     * Constructor in which you can specify your own parameters
     *
     * @param packageName The package in which to find the properties file
     * @param currentLocale The {@link Locale} with which the properties file can be associated
     * @see #generateTangoManager(int)
     * @see #changeResourceBundle(String, String, Locale)
     */
    public AttributeTableSelectionBean(String packageName, final Locale currentLocale) {
        super();
        if (packageName == null) {
            packageName = PACKAGE_NAME;
        }
        initResourceBundle(packageName, currentLocale);
        setTangoManager(generateTangoManager(DUMMY));
        selectionPanel = new AttributesSelectionTablePanel(this);
        registerManagerListener();
    }

    /**
     * Returns the main panel managed by this bean.
     *
     * @return an {@link AttributesSelectionTablePanel}
     */
    public AttributesSelectionTablePanel getSelectionPanel() {
        return selectionPanel;
    }

    /**
     * Returns the {@link ITangoEntitiesSelectionManager} associated with this
     * bean. This is for experts and should be used wisely. This method is used
     * by UI.
     *
     * @return an {@link ITangoEntitiesSelectionManager}
     */
    public ITangoEntitiesSelectionManager getTangoManager() {
        return tangoManager;
    }

    /**
     * Associates an {@link ITangoEntitiesSelectionManager} with this {@link AttributeTableSelectionBean}.
     *
     * @param tangoManager
     *            the {@link ITangoEntitiesSelectionManager} to set.
     */
    public void setTangoManager(final ITangoEntitiesSelectionManager tangoManager) {
        if (this.tangoManager != tangoManager) {
            unregisterManagerListener();
            this.tangoManager = tangoManager;
            registerManagerListener();
            if (selectionPanel != null) {
                selectionPanel.getDomainComboBox().setDomains();
            }
        }
    }

    private void initResourceBundle(final String packageName, final Locale currentLocale) {
        messageManager = new MessageManager(packageName);
        messageManager.initResourceBundle(currentLocale);
    }

    /**
     * Forces this bean to change its message sources, and updates the texts in
     * UI.
     *
     * @param packageName The package in which the properties file can be found
     * @param currentLocale The properties file associated {@link Locale} (can be <code>null</code>).
     */
    public void changeResourceBundle(final String packageName, final Locale currentLocale) {
        initResourceBundle(packageName, currentLocale);
        selectionPanel.refreshTexts();
    }

    /**
     * Method used by UI to find the different texts.
     *
     * @param key A key to get the expected text
     * @return a {@link String}
     */
    public String getMessage(final String key) {
        return messageManager.getMessage(key);
    }

    private void registerManagerListener() {
        registerListener(tangoManager);
    }

    private void registerListener(final ITangoEntitiesSelectionManager selectionManager) {
        if (selectionManager instanceof BufferedTangoEntitiesSelectionManager) {
            ((BufferedTangoEntitiesSelectionManager) selectionManager).register(selectionPanel);
        } else if (tangoManager instanceof FilteredTangoEntitiesSelectionManager) {
            registerListener(((FilteredTangoEntitiesSelectionManager) selectionManager).getWrappedTasm());
        }
    }

    private void unregisterManagerListener() {
        unregisterListener(tangoManager);
    }

    private void unregisterListener(final ITangoEntitiesSelectionManager selectionManager) {
        if (selectionManager instanceof BufferedTangoEntitiesSelectionManager) {
            ((BufferedTangoEntitiesSelectionManager) selectionManager).unregister(selectionPanel);
        } else if (selectionManager instanceof FilteredTangoEntitiesSelectionManager) {
            unregisterListener(((FilteredTangoEntitiesSelectionManager) selectionManager).getWrappedTasm());
        }
    }

    private void startManager(final ITangoEntitiesSelectionManager selectionManager) {
        if (selectionManager instanceof BufferedTangoEntitiesSelectionManager) {
            ((BufferedTangoEntitiesSelectionManager) selectionManager).start();
        } else if (selectionManager instanceof FilteredTangoEntitiesSelectionManager) {
            startManager(((FilteredTangoEntitiesSelectionManager) selectionManager).getWrappedTasm());
        }
    }

    private void stopManager(final ITangoEntitiesSelectionManager selectionManager) {
        if (selectionManager instanceof BufferedTangoEntitiesSelectionManager) {
            ((BufferedTangoEntitiesSelectionManager) selectionManager).cancel();
        } else if (selectionManager instanceof FilteredTangoEntitiesSelectionManager) {
            stopManager(((FilteredTangoEntitiesSelectionManager) selectionManager).getWrappedTasm());
        }
    }

    /**
     * Cleans this bean and its UI
     */
    public void clean() {
        unregisterManagerListener();
        selectionPanel.clean();
        selectionPanel = null;
        tangoManager = null;
        messageManager = null;
    }

    public Attribute[] validateTableAndGetAttributes() {
        if (selectionPanel != null && selectionPanel.getAttributesSelectTable() != null
                && selectionPanel.getAttributesSelectTable().getModel() != null) {
            selectionPanel.getAttributesSelectTable().applyChange();
            selectionPanel.getAttributesSelectTable().getModel().removeNotSelectedRows();
            return selectionPanel.getAttributesSelectTable().getModel().getRows();
        } else {
            return null;
        }
    }

    @Override
    public void start() {
        startManager(getTangoManager());
    }

    @Override
    public void stop() {
        stopManager(getTangoManager());
    }

    /**
     * Generates an {@link ITangoEntitiesSelectionManager} according to an expected type.
     *
     * @param tangoManagerType the expected type
     * @return an {@link ITangoEntitiesSelectionManager}
     * @see #BASIC
     * @see #SORTED
     * @see #BUFFERED
     * @see #BUFFERED_AND_SORTED
     * @see #DUMMY
     */
    public static ITangoEntitiesSelectionManager generateTangoManager(final int tangoManagerType) {
        ITangoEntitiesSelectionManager manager;
        switch (tangoManagerType) {
            case BASIC:
                manager = new BasicTangoEntitiesSelectionManager();
                break;
            case SORTED:
                manager = new SortedTangoEntitiesSelectionManager(new BasicTangoEntitiesSelectionManager());
                break;
            case BUFFERED:
                manager = new ThreadedBufferedTangoEntitiesSelectionManager(new BasicTangoEntitiesSelectionManager());
                break;
            case BUFFERED_AND_SORTED:
                manager = new SortedTangoEntitiesSelectionManager(
                        new ThreadedBufferedTangoEntitiesSelectionManager(new BasicTangoEntitiesSelectionManager()));
                break;
            default:
                manager = new DummyTangoEntitiesSelectionManager();
                break;
        }
        return manager;
    }

}
