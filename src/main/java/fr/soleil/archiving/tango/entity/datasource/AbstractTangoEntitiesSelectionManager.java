package fr.soleil.archiving.tango.entity.datasource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.archiving.tango.entity.exception.EntityException;

public abstract class AbstractTangoEntitiesSelectionManager implements ITangoEntitiesSelectionManager {

    protected static final Logger LOGGER = LoggerFactory.getLogger(AbstractTangoEntitiesSelectionManager.class);

    protected TangoDbSearchAdapter wrappedTangoDbSearch;

    public AbstractTangoEntitiesSelectionManager() {
        super();
        wrappedTangoDbSearch = new TangoDbSearchAdapter();
    }

    @Override
    public DeviceClass[] loadDeviceClasses(final Domain[] domains) throws EntityException {

        if (domains == null || domains.length == 0) {
            return null;
        }
        final int numberOfDomains = domains.length;

        final List<DeviceClass[]> deviceClassesSubSets = new ArrayList<DeviceClass[]>(numberOfDomains);
        int totalNumberOfDeviceClasses = 0;

        for (int i = 0; i < numberOfDomains; i++) {
            final Domain currentDomain = domains[i];

            final DeviceClass[] currentDeviceClassesSubset = this.loadDeviceClasses(currentDomain);
            final int sizeOfCurrentDeviceClassesSubset = currentDeviceClassesSubset == null ? 0
                    : currentDeviceClassesSubset.length;
            for (int j = 0; j < sizeOfCurrentDeviceClassesSubset; j++) {
                currentDeviceClassesSubset[j].setDomain(currentDomain);
            }

            totalNumberOfDeviceClasses += sizeOfCurrentDeviceClassesSubset;
            if (sizeOfCurrentDeviceClassesSubset != 0) {
                deviceClassesSubSets.add(currentDeviceClassesSubset);
            }
        }
        final DeviceClass[] ret = AbstractTangoEntitiesSelectionManager
                .buildDeviceClassesList(totalNumberOfDeviceClasses, deviceClassesSubSets);
        return ret;
    }

    private static DeviceClass[] buildDeviceClassesList(final int totalNumberOfDeviceClasses,
            final List<DeviceClass[]> deviceClassesSubSets) {

        DeviceClass[] ret = new DeviceClass[totalNumberOfDeviceClasses];

        int k = 0;
        for (int i = 0; i < deviceClassesSubSets.size(); i++) {
            final DeviceClass[] currentDeviceClassesSubset = deviceClassesSubSets.get(i);
            final int sizeOfCurrentDeviceClassesSubset = currentDeviceClassesSubset.length;

            for (int j = 0; j < sizeOfCurrentDeviceClassesSubset; j++) {
                ret[k] = currentDeviceClassesSubset[j];
                k++;
            }
        }

        final Map<String, DeviceClass> uniqueNames = new HashMap<String, DeviceClass>();
        for (int i = 0; i < totalNumberOfDeviceClasses; i++) {
            DeviceClass alreadyAccountedForDeviceClass = uniqueNames.get(ret[i].getName());
            if (alreadyAccountedForDeviceClass == null) {
                alreadyAccountedForDeviceClass = ret[i];
            } else {
                final Collection<Member> membersToAdd = ret[i].getDevices();
                alreadyAccountedForDeviceClass.addDevices(membersToAdd);
            }
            uniqueNames.put(alreadyAccountedForDeviceClass.getName(), alreadyAccountedForDeviceClass);
        }
        ret = uniqueNames.values().toArray(new DeviceClass[uniqueNames.size()]);
        return ret;
    }
}
