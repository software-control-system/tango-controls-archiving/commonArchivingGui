package fr.soleil.archiving.tango.entity.datasource;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.esrf.TangoApi.ApiUtil;
import fr.esrf.TangoApi.Database;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Member;
import fr.soleil.archiving.tango.entity.exception.EntityException;
import fr.soleil.lib.project.ObjectUtils;

public class BasicTangoEntitiesSelectionManager extends AbstractTangoEntitiesSelectionManager {

    private static final String DSERVER = "dserver";
    private static final String FAILED_WHILE_EXECUTING_ARCHIVING_MANAGER_API_GET_DOMAINS_METHOD = "Failed while executing ArchivingManagerApi.getDomains() method...";

    public BasicTangoEntitiesSelectionManager() {
        super();
    }

    @Override
    public DeviceClass[] loadDeviceClasses(Domain domain) throws EntityException {
        DeviceClass[] ret;
        if (domain == null) {
            ret = null;
        } else {
            String domainName = domain.getName();
            Map<String, Collection<String>> hashtable = wrappedTangoDbSearch.getClassAndDevices(domainName);
            if (hashtable == null) {
                ret = null;
            } else {
                ret = new DeviceClass[hashtable.size()];
                int j = 0;
                for (Entry<String, Collection<String>> entry : hashtable.entrySet()) {
                    String nextClass = entry.getKey();
                    Collection<String> devices = entry.getValue();
                    ret[j] = new DeviceClass(nextClass);
                    for (String dev : devices) {
                        ret[j].addDevice(new Member(dev));
                    }
                    j++;
                }
            }
        }
        return ret;
    }

    @Override
    public Attribute[] loadAttributes(Domain domain, DeviceClass deviceClass) throws EntityException {
        Attribute[] ret = null;
        if ((domain != null) && (deviceClass != null)) {
            String domainName = domain.getName();
            String deviceClassName = deviceClass.getName();
            Map<String, Collection<String>> hashtable = wrappedTangoDbSearch.getClassAndDevices(domainName);
            Collection<String> devicesList = hashtable.get(deviceClassName);
            if (devicesList != null) {
                String[] att = wrappedTangoDbSearch.getAttributesForClass(deviceClassName, devicesList);
                if (att != null) {
                    ret = new Attribute[att.length];
                    for (int i = 0; i < att.length; i++) {
                        ret[i] = new Attribute();
                        ret[i].setName(att[i]);
                        ret[i].setDeviceClass(deviceClass.getName());
                    }
                }
            }
        }
        return ret;
    }

    @Override
    public Domain[] loadDomains() throws EntityException {
        Collection<String> domains = wrappedTangoDbSearch.getDomains();
        Domain[] ret;
        if (domains == null) {
            ret = null;
        } else {
            ret = new Domain[domains.size()];
            int i = 0;
            for (String domain : domains) {
                ret[i++] = new Domain(domain);
            }
        }
        return ret;
    }

    @Override
    public Attribute[] loadAttributes(DeviceClass deviceClass, Attribute brother) {
        Attribute[] ret;
        if (brother == null || deviceClass == null) {
            ret = null;
        } else {
            Collection<Member> members = deviceClass.getDevices();
            ret = new Attribute[members.size()];
            int i = 0;
            for (Member nextDevice : members) {
                StringTokenizer st = new StringTokenizer(nextDevice.getName(), GUIUtilities.TANGO_DELIM);
                String domainName = st.nextToken();
                String familyName = st.nextToken();
                String memberName = st.nextToken();

                String completeName = nextDevice.getName() + GUIUtilities.TANGO_DELIM + brother.getName();

                ret[i] = new Attribute();
                ret[i].setDomain(domainName);
                ret[i].setFamily(familyName);
                ret[i].setDevice(memberName);
                ret[i].setMember(memberName);
                ret[i].setName(brother.getName());
                ret[i].setCompleteName(completeName);
                ret[i].setDeviceClass(deviceClass.getName());
                i++;
            }
        }
        return ret;
    }

    @Override
    public Domain[] loadDomains(String domainsRegExp) throws EntityException {
        Collection<String> domains = wrappedTangoDbSearch.getDomains(domainsRegExp);
        Domain[] ret;
        if (domains == null) {
            ret = null;
        } else {
            ret = new Domain[domains.size()];
            int i = 0;
            for (String domain : domains) {
                ret[i++] = new Domain(domain);
            }
        }
        return ret;
    }

    public Collection<String> getDomains(String domainsRegExp) throws EntityException {
        try {
            Database database = ApiUtil.get_db_obj();
            Collection<String> domains = new ArrayList<String>();
            String[] res_domains = database.get_device_domain(domainsRegExp);
            for (int i = 0; i < res_domains.length; i++) {
                String res_domain = res_domains[i];
                if (!DSERVER.equals(res_domain)) {
                    domains.add(res_domain);
                }
            }
            return domains;
        } catch (DevFailed devFailed) {
//            System.out.println("ArchivingManagerApi.getDomains");
            String message = GlobalConst.ARCHIVING_ERROR_PREFIX;
            String reason = GlobalConst.TANGO_COMM_EXCEPTION;
            String desc = FAILED_WHILE_EXECUTING_ARCHIVING_MANAGER_API_GET_DOMAINS_METHOD;
            throw new EntityException(message, reason, ErrSeverity.WARN, desc, ObjectUtils.EMPTY_STRING, devFailed);
        }
    }

}
