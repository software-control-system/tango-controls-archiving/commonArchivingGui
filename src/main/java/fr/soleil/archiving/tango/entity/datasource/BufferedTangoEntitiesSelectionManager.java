package fr.soleil.archiving.tango.entity.datasource;

import java.util.HashMap;
import java.util.Map;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.IPreBufferingEventListener;
import fr.soleil.archiving.tango.entity.exception.EntityException;
import fr.soleil.lib.project.ObjectUtils;
import fr.soleil.lib.project.date.DateUtil;

public class BufferedTangoEntitiesSelectionManager extends FilteredTangoEntitiesSelectionManager {

    private static final String KEY_SEPARATOR = "__|__";
    private static final String BUFFERING_TANGO_DEVICES_AND_CLASSES = "buffering tango devices and classes...";
    private static final String BUFFERING_TANGO_DEVICES_AND_CLASSES_DONE_IN = "buffering tango devices and classes done in ";
    private static final String MEMORY_ERROR = "memory error";
    private static final String UNEXPECTED_ERROR = "unexpected error";

    private final Object domainsBufferLock;
    private Domain[] domainsBuffer;
    private final Map<String, Domain[]> domainsRegExpToDomainsBuffer;
    private final Map<String, DeviceClass[]> domainToDeviceClassesBuffer;
    private final Map<String, Attribute[]> domainAndDeviceClassToAttributeBuffer;

    private IPreBufferingEventListener preBufferingEventListener;
    private volatile boolean started;
    private volatile boolean canceled;

    public BufferedTangoEntitiesSelectionManager(final ITangoEntitiesSelectionManager tasm) {
        super(tasm);
        started = false;
        canceled = false;
        preBufferingEventListener = null;
        domainsBufferLock = new Object();
        domainsBuffer = new Domain[0];
        domainsRegExpToDomainsBuffer = new HashMap<>();
        domainToDeviceClassesBuffer = new HashMap<>();
        domainAndDeviceClassToAttributeBuffer = new HashMap<>();
    }

    @Override
    public Domain[] loadDomains(final String domainsRegExp) throws EntityException {
        synchronized (this.domainsRegExpToDomainsBuffer) {
            // Other threads have to wait for the completion of the buffering,
            // so that the loading from database isn't done twice
            Domain[] bufferedDomains = this.domainsRegExpToDomainsBuffer.get(domainsRegExp);

            if (bufferedDomains == null || bufferedDomains.length == 0) {
                bufferedDomains = super.loadDomains(domainsRegExp);
                try {
                    domainsRegExpToDomainsBuffer.put(domainsRegExp, bufferedDomains);
                } catch (final OutOfMemoryError t) {
                    LOGGER.error(MEMORY_ERROR, t);
                } catch (final Throwable t) {
                    LOGGER.error(UNEXPECTED_ERROR, t);
                }
            }

            return bufferedDomains;
        }
    }

    @Override
    public Domain[] loadDomains() throws EntityException {
        Domain[] domains;
        synchronized (domainsBufferLock) {
            // Other threads have to wait for the completion of the buffering,
            // so that the loading from database isn't done twice
            domains = this.domainsBuffer;
            if (domains == null || domains.length == 0) {
                domains = super.loadDomains();
                this.domainsBuffer = domains;
            }
        }
        return domains;
    }

    @Override
    public DeviceClass[] loadDeviceClasses(final Domain domain) throws EntityException {
        final String key = domain.getName();
        DeviceClass[] deviceClasses;
        synchronized (domainToDeviceClassesBuffer) {
            // Other threads have to wait for the completion of the buffering,
            // so that the loading from database isn't done twice
            deviceClasses = this.domainToDeviceClassesBuffer.get(key);
            try {
                if (deviceClasses == null) {
                    deviceClasses = super.loadDeviceClasses(domain);
                    domainToDeviceClassesBuffer.put(key, deviceClasses);
                }
            } catch (final OutOfMemoryError t) {
                LOGGER.error(MEMORY_ERROR, t);
            } catch (final Throwable t) {
                LOGGER.error(UNEXPECTED_ERROR, t);
            }
        }
        return deviceClasses;
    }

    @Override
    public Attribute[] loadAttributes(final Domain domain, final DeviceClass deviceClass) throws EntityException {
        final String key = domain.getName() + KEY_SEPARATOR + deviceClass.getName();

        synchronized (this.domainAndDeviceClassToAttributeBuffer) {
            // Other threads have to wait for the completion of the buffering,
            // so that the loading from database isn't done twice
            Attribute[] attributes = this.domainAndDeviceClassToAttributeBuffer.get(key);

            try {
                if (attributes == null) {
                    attributes = super.loadAttributes(domain, deviceClass);
                    domainAndDeviceClassToAttributeBuffer.put(key, attributes);
                }
            } catch (final OutOfMemoryError t) {
                LOGGER.error(MEMORY_ERROR, t);
            } catch (final Throwable t) {
                LOGGER.error(UNEXPECTED_ERROR, t);
            }

            return attributes;
        }
    }

    protected void doPreBuffering() {
        LOGGER.info(BUFFERING_TANGO_DEVICES_AND_CLASSES);
        final long startTime = System.currentTimeMillis();
        try {
            started = true;
            canceled = false;
            final Domain[] doms = this.loadDomains();
            if (doms != null) {
                final int totalSteps = doms.length;
                for (int i = 0; i < totalSteps && !canceled; i++) {
                    loadDeviceClasses(doms[i]);
                    notifyStepDone(i + 1, totalSteps);
                }
            }
        } catch (final EntityException e) {
            LOGGER.error(ObjectUtils.EMPTY_STRING, e);
        } finally {
            final long endTime = System.currentTimeMillis();
            LOGGER.info(DateUtil.elapsedTimeToStringBuilder(
                    new StringBuilder(BUFFERING_TANGO_DEVICES_AND_CLASSES_DONE_IN), endTime - startTime).toString());
            started = false;
            this.notifyAllDone();
        }
    }

    public void register(final IPreBufferingEventListener source) {
        this.setPreBufferingEventListener(source);
    }

    private void setPreBufferingEventListener(final IPreBufferingEventListener source) {
        this.preBufferingEventListener = source;
        if (source != null) {
            if (!isStarted()) {
                if (domainsBuffer != null && domainsBuffer.length > 0) {
                    notifyStepDone(domainsBuffer.length, domainsBuffer.length);
                    notifyAllDone();
                }
            }
        }
    }

    public void unregister(final IPreBufferingEventListener source) {
        this.setPreBufferingEventListener(null);
    }

    public void notifyAllDone() {
        if (preBufferingEventListener != null) {
            preBufferingEventListener.allDone();
        }
    }

    public void notifyStepDone(final int step, final int totalSteps) {
        if (preBufferingEventListener != null) {
            preBufferingEventListener.stepDone(step, totalSteps);
        }
    }

    public boolean isStarted() {
        return started;
    }

    public void start() {
        if (!started) {
            doPreBuffering();
        }
    }

    public boolean isCanceled() {
        return canceled;
    }

    public void cancel() {
        canceled = true;
    }

}
