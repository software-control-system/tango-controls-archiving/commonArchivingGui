package fr.soleil.archiving.tango.entity.datasource;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.exception.EntityException;

public class DummyTangoEntitiesSelectionManager implements ITangoEntitiesSelectionManager {

    public DummyTangoEntitiesSelectionManager() {
        super();
    }

    @Override
    public Attribute[] loadAttributes(final Domain domain, final DeviceClass deviceClass) throws EntityException {
        return null;
    }

    @Override
    public Attribute[] loadAttributes(final DeviceClass deviceClass, final Attribute brother) {
        return null;
    }

    @Override
    public DeviceClass[] loadDeviceClasses(final Domain domain) throws EntityException {
        return null;
    }

    @Override
    public DeviceClass[] loadDeviceClasses(final Domain[] domains) throws EntityException {
        return null;
    }

    @Override
    public Domain[] loadDomains(final String domainsRegExp) throws EntityException {
        return null;
    }

    @Override
    public Domain[] loadDomains() throws EntityException {
        return null;
    }

}
