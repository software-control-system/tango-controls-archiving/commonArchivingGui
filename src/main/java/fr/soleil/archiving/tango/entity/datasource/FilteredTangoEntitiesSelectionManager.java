package fr.soleil.archiving.tango.entity.datasource;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.exception.EntityException;

public class FilteredTangoEntitiesSelectionManager extends AbstractTangoEntitiesSelectionManager {

    private final ITangoEntitiesSelectionManager wrappedTasm;

    public FilteredTangoEntitiesSelectionManager(ITangoEntitiesSelectionManager tasm) {
        super();
        this.wrappedTasm = tasm;
    }

    @Override
    public fr.soleil.archiving.tango.entity.Domain[] loadDomains(String domainsRegExp) throws EntityException {
        return this.wrappedTasm.loadDomains(domainsRegExp);
    }

    @Override
    public Domain[] loadDomains() throws EntityException {
        return this.wrappedTasm.loadDomains();
    }

    @Override
    public DeviceClass[] loadDeviceClasses(Domain domain) throws EntityException {
        return this.wrappedTasm.loadDeviceClasses(domain);
    }

    @Override
    public Attribute[] loadAttributes(Domain domain, DeviceClass deviceClass) throws EntityException {
        return this.wrappedTasm.loadAttributes(domain, deviceClass);
    }

    @Override
    public Attribute[] loadAttributes(DeviceClass deviceClass, Attribute brother) {
        return this.wrappedTasm.loadAttributes(deviceClass, brother);
    }

    public ITangoEntitiesSelectionManager getWrappedTasm() {
        return wrappedTasm;
    }

}
