package fr.soleil.archiving.tango.entity.datasource;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.exception.EntityException;

public interface ITangoEntitiesSelectionManager {

    public Domain[] loadDomains(String domainsRegExp) throws EntityException;

    public Domain[] loadDomains() throws EntityException;

    public DeviceClass[] loadDeviceClasses(Domain domain) throws EntityException;

    public DeviceClass[] loadDeviceClasses(Domain[] domains) throws EntityException;

    public Attribute[] loadAttributes(Domain domain, DeviceClass deviceClass) throws EntityException;

    public Attribute[] loadAttributes(DeviceClass deviceClass, Attribute brother);

    /**
     * Returns the {@link ILogger} associated with this {@link ITangoEntitiesSelectionManager}
     *
     * @return an {@link ILogger}
     */
    // public ILogger getLogger();

    /**
     * Associates an {@link ILogger} with this {@link ITangoEntitiesSelectionManager}
     *
     * @param logger
     *            The desired {@link ILogger}
     */
    // public void setLogger(ILogger logger);
}
