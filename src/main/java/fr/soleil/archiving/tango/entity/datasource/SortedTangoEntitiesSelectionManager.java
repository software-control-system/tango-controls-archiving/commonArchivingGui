package fr.soleil.archiving.tango.entity.datasource;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.exception.EntityException;
import fr.soleil.archiving.tango.entity.ui.comparator.EntitiesComparator;

public class SortedTangoEntitiesSelectionManager extends FilteredTangoEntitiesSelectionManager {

    private final EntitiesComparator comparator;

    public SortedTangoEntitiesSelectionManager(ITangoEntitiesSelectionManager _tasm) {
        super(_tasm);

        this.comparator = new EntitiesComparator();
    }

    @Override
    public DeviceClass[] loadDeviceClasses(Domain domain) throws EntityException {
        DeviceClass[] unorderedValues = super.loadDeviceClasses(domain);
        if (unorderedValues == null || unorderedValues.length == 0) {
            return null;
        }

        List<DeviceClass> list = Arrays.asList(unorderedValues);
        Collections.sort(list, this.comparator);

        DeviceClass[] ret = list.toArray(unorderedValues);
        return ret;
    }

    @Override
    public DeviceClass[] loadDeviceClasses(Domain[] domains) throws EntityException {
        DeviceClass[] unorderedValues = super.loadDeviceClasses(domains);
        if (unorderedValues == null || unorderedValues.length == 0) {
            return null;
        }

        List<DeviceClass> list = Arrays.asList(unorderedValues);
        Collections.sort(list, this.comparator);

        DeviceClass[] ret = list.toArray(unorderedValues);
        return ret;
    }

    @Override
    public Attribute[] loadAttributes(Domain domain, DeviceClass deviceClass) throws EntityException {
        Attribute[] unorderedValues = super.loadAttributes(domain, deviceClass);
        if (unorderedValues == null || unorderedValues.length == 0) {
            return null;
        }

        List<Attribute> list = Arrays.asList(unorderedValues);
        Collections.sort(list, this.comparator);

        Attribute[] ret = list.toArray(unorderedValues);
        return ret;
    }

    @Override
    public Domain[] loadDomains() throws EntityException {
        Domain[] unorderedValues = super.loadDomains();
        if (unorderedValues == null || unorderedValues.length == 0) {
            return null;
        }

        List<Domain> list = Arrays.asList(unorderedValues);
        Collections.sort(list, this.comparator);

        Domain[] ret = list.toArray(unorderedValues);
        return ret;
    }

    @Override
    public Attribute[] loadAttributes(DeviceClass deviceClass, Attribute brother) {
        Attribute[] unorderedValues = super.loadAttributes(deviceClass, brother);
        if (unorderedValues == null || unorderedValues.length == 0) {
            return null;
        }

        List<Attribute> list = Arrays.asList(unorderedValues);
        Collections.sort(list, this.comparator);

        Attribute[] ret = list.toArray(unorderedValues);
        return ret;
    }

    @Override
    public Domain[] loadDomains(String domainsRegExp) throws EntityException {
        Domain[] unorderedValues = super.loadDomains(domainsRegExp);
        if (unorderedValues == null || unorderedValues.length == 0) {
            return null;
        }

        List<Domain> list = Arrays.asList(unorderedValues);
        Collections.sort(list, this.comparator);

        Domain[] ret = list.toArray(unorderedValues);
        return ret;
    }
}
