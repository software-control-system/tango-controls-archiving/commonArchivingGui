package fr.soleil.archiving.tango.entity.datasource;

import java.util.Collection;
import java.util.Map;

import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.common.api.tools.GlobalConst;
import fr.soleil.archiving.common.api.tools.TangoDbSearch;
import fr.soleil.archiving.tango.entity.exception.EntityException;
import fr.soleil.lib.project.ObjectUtils;

/**
 * This class wraps the TangoDbSearch class by generating an {@link EntityException} in case of {@link DevFailed}
 * 
 * @author PIERREJOSEPH
 */
public class TangoDbSearchAdapter {

    // Wrapped object
    private TangoDbSearch wrappedTangoDbSearch = null;

    public TangoDbSearchAdapter() {
        wrappedTangoDbSearch = new TangoDbSearch();
    }

    public String[] getAttributesForClass(String classe, Collection<String> devOfDomain) throws EntityException {
        try {
            return wrappedTangoDbSearch.getAttributesForClass(classe, devOfDomain);
        } catch (DevFailed e) {
            String message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.DBT_EXCEPTION;
            String reason = GlobalConst.TANGO_COMM_EXCEPTION;
            String desc = "Failed while executing TangoDbSearchAdapter.getAttributesForClass() method...";
            throw new EntityException(message, reason, ErrSeverity.ERR, desc, ObjectUtils.EMPTY_STRING, e);
        }
    }

    public Collection<String> getDomains() throws EntityException {
        try {
            return wrappedTangoDbSearch.getDomains();
        } catch (DevFailed e) {
            String message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.DBT_EXCEPTION;
            String reason = GlobalConst.TANGO_COMM_EXCEPTION;
            String desc = "Failed while executing TangoDbSearchAdapter.getDomains() method...";
            throw new EntityException(message, reason, ErrSeverity.ERR, desc, ObjectUtils.EMPTY_STRING, e);
        }
    }

    public Collection<String> getDomains(String domainsRegExp) throws EntityException {
        try {
            return wrappedTangoDbSearch.getDomains(domainsRegExp);
        } catch (DevFailed e) {
            String message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.DBT_EXCEPTION;
            String reason = GlobalConst.TANGO_COMM_EXCEPTION;
            String desc = "Failed while executing TangoDbSearchAdapter.getDomains(domain) method...";
            throw new EntityException(message, reason, ErrSeverity.ERR, desc, ObjectUtils.EMPTY_STRING, e);
        }
    }

    public Map<String, Collection<String>> getClassAndDevices(String domain) throws EntityException {
        try {
            return wrappedTangoDbSearch.getClassAndDevices(domain);
        } catch (DevFailed e) {
            String message = GlobalConst.ARCHIVING_ERROR_PREFIX + " : " + GlobalConst.DBT_EXCEPTION;
            String reason = GlobalConst.TANGO_COMM_EXCEPTION;
            String desc = "Failed while executing TangoDbSearchAdapter.getClassAndDevices(domain) method...";
            throw new EntityException(message, reason, ErrSeverity.ERR, desc, ObjectUtils.EMPTY_STRING, e);
        }
    }
}
