package fr.soleil.archiving.tango.entity.datasource;

import fr.soleil.archiving.tango.entity.IPreBufferingEventSource;

public class ThreadedBufferedTangoEntitiesSelectionManager extends BufferedTangoEntitiesSelectionManager
        implements IPreBufferingEventSource {

    public ThreadedBufferedTangoEntitiesSelectionManager(ITangoEntitiesSelectionManager tasm) {
        super(tasm);
    }

    @Override
    protected void doPreBuffering() {
        DoPreBufferingRunnable preBufferingRunnable = new DoPreBufferingRunnable();
        Thread preBufferingThread = new Thread(preBufferingRunnable, "ArchivingGUIPreBufferingThread");

        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        preBufferingThread.setPriority(Thread.MIN_PRIORITY);

        preBufferingThread.start();
    }

    private class DoPreBufferingRunnable implements Runnable {

        @Override
        public synchronized void run() {
            ThreadedBufferedTangoEntitiesSelectionManager.super.doPreBuffering();
        }
    }

}
