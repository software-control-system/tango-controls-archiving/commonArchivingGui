package fr.soleil.archiving.tango.entity.editor;

import java.awt.Component;

import javax.swing.AbstractCellEditor;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.TableCellEditor;

import fr.soleil.archiving.tango.entity.ui.AttributesSelectTable;

/**
 * Cell editor used by {@link AttributesSelectTable}. The used component for
 * scalar attributes is a TextField. *
 */
public class AttributesSelectTableEditor extends AbstractCellEditor implements TableCellEditor {

    private static final long serialVersionUID = 2031076980469980778L;

    private JCheckBox buffer;

    /**
     * Default constructor
     */
    public AttributesSelectTableEditor() {
        buffer = null;
    }

    @Override
    public Component getTableCellEditorComponent(JTable table, Object value, boolean isSelected, int row, int column) {
        buffer = new JCheckBox();
        Boolean _value = (Boolean) value;
        buffer.setSelected(_value.booleanValue());

        return buffer;
    }

    @Override
    public Object getCellEditorValue() {
        return new Boolean(buffer.isSelected());
    }

}
