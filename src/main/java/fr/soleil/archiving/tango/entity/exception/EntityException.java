package fr.soleil.archiving.tango.entity.exception;

import fr.esrf.Tango.DevError;
import fr.esrf.Tango.DevFailed;
import fr.esrf.Tango.ErrSeverity;
import fr.soleil.archiving.gui.tools.GUIUtilities;
import fr.soleil.lib.project.ObjectUtils;

public class EntityException extends Exception {

    private static final long serialVersionUID = 3254714941928108683L;

    private static final String ERROR = "ERROR";
    private static final String PANIC = "PANIC";
    private static final String WARNING = "WARNING";
    private static final String MESSAGE = "Message : ";
    private static final String INDEX_START = "\t [";
    private static final String INDEX_END = "] : ";
    private static final String REASON = "Reason : ";
    private static final String SEVERITY = "Severity : ";
    private static final String DESCRIPTION = "Description : ";
    private static final String ORIGIN = "Origin : ";
    private static final String TAB = "\t";
    private static final String DEV_FAILED_EXCEPTION = "DevFailed EXCEPTION";
    private static final String UNKNOWN_REASON = "Unknown reason";
    private static final String UNKNOWN_EXCEPTION = "Unknown exception";
    private static final String UNKNOWN_ORIGIN = "Unknown origin";
    private static final String TIMEOUT = "org.omg.CORBA.TIMEOUT";

    private String archExcepMessage;
    private DevError[] devErrorTab;
    private boolean isDueToATimeOut = false;

    public EntityException() {
        super();
        archExcepMessage = ObjectUtils.EMPTY_STRING;
        devErrorTab = null;
    }

    public EntityException(String message) {
        super(message);
        archExcepMessage = message;
        String reason = UNKNOWN_REASON;
        ErrSeverity archSeverity = ErrSeverity.WARN;
        String desc = UNKNOWN_EXCEPTION;
        String origin = this.getClass().toString();
        DevError devError = new DevError(reason, archSeverity, desc, origin);
        devErrorTab = new DevError[1];
        devErrorTab[0] = devError;
    }

    /**
     * This class can be instanciated when exceptions in the archiving service.
     * Exceptions can be : ConnectionException, ATKException
     */
    public EntityException(String message, String reason, ErrSeverity archSeverity, String desc, String origin) {
        super(message);
        archExcepMessage = message;
        String _reason = (reason.equals(null) || reason.isEmpty()) ? UNKNOWN_REASON : reason;
        ErrSeverity _archSeverity = (archSeverity == null) ? ErrSeverity.WARN : archSeverity;
        String _desc = (desc.equals(null) || desc.isEmpty()) ? UNKNOWN_EXCEPTION : desc;
        String _origin = (origin.equals(null) || origin.isEmpty()) ? UNKNOWN_ORIGIN : origin;
        DevError devError = new DevError(_reason, _archSeverity, _desc, _origin);
        devErrorTab = new DevError[1];
        devErrorTab[0] = devError;
    }

    public EntityException(String message, String reason, ErrSeverity archSeverity, String desc, String origin,
            Exception e) {
        super(e);
        archExcepMessage = message;

        if (e instanceof DevFailed) {
            // The current stack is initialized
            devErrorTab = new DevError[((DevFailed) e).errors.length + 1];
            // The stack of the catched error copied
            System.arraycopy(((DevFailed) e).errors, 0, devErrorTab, 0, ((DevFailed) e).errors.length);
            // A new DevError object is built with the given parameters
            String _reason = (reason.equals(null) || reason.isEmpty()) ? UNKNOWN_REASON : reason;
            ErrSeverity _archSeverity = (archSeverity == null) ? ErrSeverity.WARN : archSeverity;
            String _desc = (desc.equals(null) || desc.isEmpty()) ? DEV_FAILED_EXCEPTION : desc;
            String _origin = (origin.equals(null) || origin.isEmpty()) ? e.getClass().getName() : origin;
            DevError devError = new DevError(_reason, _archSeverity, _desc, _origin);
            // The DevError object is added at the end of the stack
            devErrorTab[devErrorTab.length - 1] = devError;
        } else if (e instanceof EntityException) {
            // The current stack is initialized
            devErrorTab = new DevError[((EntityException) e).devErrorTab.length + 1];
            // The stack of the catched error copied
            System.arraycopy(((EntityException) e).devErrorTab, 0, devErrorTab, 0,
                    ((EntityException) e).devErrorTab.length);
            // A new DevError object is built with the given parameters
            String _reason = (reason.equals(null) || reason.isEmpty()) ? UNKNOWN_REASON : reason;
            ErrSeverity _archSeverity = (archSeverity == null) ? ErrSeverity.WARN : archSeverity;
            String _desc = (desc.equals(null) || desc.isEmpty()) ? UNKNOWN_EXCEPTION : desc;
            String _origin = (origin.equals(null) || origin.isEmpty()) ? e.getClass().getName() : origin;
            DevError devError = new DevError(_reason, _archSeverity, _desc, _origin);

            // The DevError object is added at the end of the stack
            devErrorTab[devErrorTab.length - 1] = devError;
        } else {
            // The current stack is initialized
            devErrorTab = new DevError[2];
            // A new DevError object is built for the catched exception
            // (Original exception)
            String reason_original = e.getMessage();
            ErrSeverity archSeverity_original = ErrSeverity.WARN;
            String desc_original = e.getLocalizedMessage();
            String origin_original = e.getClass().getName();
            DevError devError_original = new DevError(reason_original, archSeverity_original, desc_original,
                    origin_original);
            // A new DevError object is built with the given parameters
            String _reason = (reason.equals(null) || reason.isEmpty()) ? UNKNOWN_REASON : reason;
            ErrSeverity _archSeverity = ((archSeverity == null) ? ErrSeverity.WARN : archSeverity);
            String _desc = (desc.equals(null) || desc.isEmpty()) ? UNKNOWN_EXCEPTION : desc;
            String _origin = (origin.equals(null) || origin.isEmpty()) ? e.getClass().getName() : origin;
            DevError devError = new DevError(_reason, _archSeverity, _desc, _origin);

            // The DevError objects are added at the end of the stack
            devErrorTab[0] = devError_original;
            devErrorTab[1] = devError;
        }
    }

    public void addStack(String message, EntityException e) {
        if (e.isDueToATimeOut()) {
            this.setDueToATimeOut(true);
        }

        archExcepMessage = message;

        // The current stack is cloned
        DevError[] devErrorTabClone = devErrorTab;

        if (e.devErrorTab != null) {
            if (devErrorTabClone != null) {
                // The current stack is re-initialized
                devErrorTab = new DevError[devErrorTabClone.length + e.devErrorTab.length];
                // The cloned is copied again
                System.arraycopy(devErrorTabClone, 0, devErrorTab, 0, devErrorTabClone.length);
                // The stack of the catched error copied
                System.arraycopy(e.devErrorTab, 0, devErrorTab, devErrorTabClone.length, e.devErrorTab.length);
            } else {
                // The current stack is re-initialized
                devErrorTab = new DevError[e.devErrorTab.length];
                // The stack of the catched error copied
                System.arraycopy(e.devErrorTab, 0, devErrorTab, 0, e.devErrorTab.length);
            }
        }
    }

    public void addStack(String message, String reason, ErrSeverity archSeverity, String desc, String origin,
            EntityException e) {
        if (e.isDueToATimeOut()) {
            this.setDueToATimeOut(true);
        }

        archExcepMessage = message;
        // new ArchivingException
        String _reason = (reason.equals(null) || reason.isEmpty()) ? UNKNOWN_REASON : reason;
        ErrSeverity _archSeverity = ((archSeverity == null) ? ErrSeverity.WARN : archSeverity);
        String _desc = (desc.equals(null) || desc.isEmpty()) ? UNKNOWN_EXCEPTION : desc;
        String _origin = (origin.equals(null) || origin.isEmpty()) ? e.getClass().getName() : origin;
        DevError _devError = new DevError(_reason, _archSeverity, _desc, _origin);
        // The current stack is cloned
        DevError[] devErrorTabClone = devErrorTab;

        if (e.devErrorTab != null) {
            if (devErrorTabClone != null) {
                // The current stack is re-initialized
                int devErrorTabLength = e.devErrorTab == null ? 0 : e.devErrorTab.length;
                devErrorTab = new DevError[devErrorTabClone.length + devErrorTabLength + 1];
                // devErrorTab = new DevError[ devErrorTabClone.length +
                // e.devErrorTab.length + 1 ];

                // The cloned is copied again
                System.arraycopy(devErrorTabClone, 0, devErrorTab, 0, devErrorTabClone.length);
                // The stack of the catched error copied
                System.arraycopy(e.devErrorTab, 0, devErrorTab, devErrorTabClone.length, e.devErrorTab.length);
                // The DevError builded with the given parameters is added at
                // the end of the stack
                devErrorTab[devErrorTabClone.length + e.devErrorTab.length] = _devError;
            } else {
                // The current stack is re-initialized
                devErrorTab = new DevError[e.devErrorTab.length + 1];
                // The stack of the catched error copied
                System.arraycopy(e.devErrorTab, 0, devErrorTab, 0, e.devErrorTab.length);
                // The DevError builded with the given parameters is added at
                // the end of the stack
                devErrorTab[e.devErrorTab.length] = _devError;
            }
        }
    }

    @Override
    public String getMessage() {
        return archExcepMessage;
    }

    public String getLastExceptionMessage() {
        return devErrorTab[0].desc;
    }

    @Override
    public String toString() {
        StringBuilder stringBuffer = new StringBuilder();
        stringBuffer.append(MESSAGE).append(archExcepMessage).append(GUIUtilities.CRLF);
        if (devErrorTab != null) {
            for (int i = 0; i < devErrorTab.length; i++) {
                DevError devError = devErrorTab[i];
                stringBuffer.append(INDEX_START).append(i + 1).append(INDEX_END).append(GUIUtilities.CRLF);
                stringBuffer.append(TAB).append(TAB).append(REASON).append(devError.reason).append(GUIUtilities.CRLF);
                stringBuffer.append(TAB).append(TAB).append(SEVERITY).append(errorSeverityToString(devError.severity))
                        .append(GUIUtilities.CRLF);
                stringBuffer.append(TAB).append(TAB).append(DESCRIPTION).append(devError.desc)
                        .append(GUIUtilities.CRLF);
                stringBuffer.append(TAB).append(TAB).append(ORIGIN).append(devError.origin).append(GUIUtilities.CRLF);
            }
        }
        return stringBuffer.toString();
    }

    private String errorSeverityToString(ErrSeverity errSeverity) {
        String toString;
        if (errSeverity == null) {
            toString = WARNING;
        } else {
            switch (errSeverity.value()) {
                case ErrSeverity._ERR:
                    toString = ERROR;
                    break;
                case ErrSeverity._PANIC:
                    toString = PANIC;
                    break;
                default:
                    toString = WARNING;
                    break;
            }
        }
        return toString;
    }

    public boolean isNull() {
        return (devErrorTab == null || devErrorTab.length == 0);
    }

    public DevFailed toTangoException() {
        DevFailed devFailed = new DevFailed(archExcepMessage, devErrorTab);
        return devFailed;
    }

    public boolean computeIsDueToATimeOut() {
        Throwable cause = this.getCause();
        if (cause instanceof DevFailed) {
            DevFailed devFailedCause = (DevFailed) cause;
            DevError[] errs = devFailedCause.errors;
            if (errs != null) {
                for (int i = 0; i < errs.length; i++) {
                    DevError nextErr = errs[i];
                    String reason = nextErr.reason;
                    if (reason.indexOf(TIMEOUT) != -1) {
                        this.setDueToATimeOut(true);
                        return true;
                    }
                }
            }
        }
        this.setDueToATimeOut(false);
        return false;
    }

    /**
     * @return Returns the isDueToATimeOut.
     */
    public boolean isDueToATimeOut() {
        return this.isDueToATimeOut;
    }

    /**
     * @param isDueToATimeOut
     *            The isDueToATimeOut to set.
     */
    public void setDueToATimeOut(boolean isDueToATimeOut) {
        this.isDueToATimeOut = isDueToATimeOut;
    }

}
