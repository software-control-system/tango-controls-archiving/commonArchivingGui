package fr.soleil.archiving.tango.entity.listener;

import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.esrf.Tango.DevFailed;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.exception.EntityException;
import fr.soleil.archiving.tango.entity.ui.AttributesSelectComboBox;

public class AttributesSelectItemListener implements ItemListener {

    private final AttributeTableSelectionBean selectionBean;
    private final Logger logger = LoggerFactory.getLogger(AttributesSelectItemListener.class);

    public AttributesSelectItemListener(final AttributeTableSelectionBean selectionBean) {
        super();
        this.selectionBean = selectionBean;
    }

    @Override
    public void itemStateChanged(final ItemEvent event) {
        final int stateChangeType = event.getStateChange();
        if (stateChangeType == ItemEvent.SELECTED) {
            final String selected = (String) event.getItem();
            if (selected != null && !selected.equals(AttributesSelectComboBox.NO_SELECTION)) {
                final AttributesSelectComboBox source = (AttributesSelectComboBox) event.getSource();
                final int type = source.getType();

                switch (type) {
                    case AttributesSelectComboBox.DOMAIN_TYPE:
                        try {
                            newDomainSelected(selected);
                            if (selectionBean != null) {
                                logger.debug(selectionBean
                                        .getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.DeviceClasses.Load.OK"));
                            }
                        } catch (final EntityException e1) {
                            e1.printStackTrace();
                            logger.error("exception", e1);
                            if (selectionBean != null) {
                                logger.error("exception", e1);
                                logger.error(selectionBean
                                        .getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.DeviceClasses.Load.KO"));
                            }
                        }
                        break;

                    case AttributesSelectComboBox.DEVICE_CLASS_TYPE:
                        try {
                            newDeviceClassSelected(selected);
                            if (selectionBean != null) {
                                logger.debug(selectionBean
                                        .getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.Attributes.Load.OK"));
                            }
                        } catch (final DevFailed e) {
                            e.printStackTrace();
                            logger.error("exception", e);
                            if (selectionBean != null) {
                                logger.error("exception", e);
                                logger.error(selectionBean
                                        .getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.Attributes.Load.KO"));
                            }
                        }
                        break;

                    case AttributesSelectComboBox.ATTRIBUTE_TYPE:
                        break;

                    default:
                        throw new IllegalStateException();
                }
            }
        }
    }

    /**
     * @param selected
     * @throws DevFailed
     */
    private void newDeviceClassSelected(final String selected) throws DevFailed {
        if (selectionBean != null && selectionBean.getSelectionPanel() != null) {
            final AttributesSelectComboBox domainComboBox = selectionBean.getSelectionPanel().getDomainComboBox();
            final String currentDomainName = (String) domainComboBox.getSelectedItem();
            final AttributesSelectComboBox deviceClassComboBox = selectionBean.getSelectionPanel()
                    .getDeviceClassComboBox();
            DeviceClass selectedDeviceClass = deviceClassComboBox.getSelectedDeviceClass();
            if (selectedDeviceClass == null) {
                selectedDeviceClass = new DeviceClass(selected);
            }
            Domain correspondingDomain = selectedDeviceClass.getDomain();
            if (correspondingDomain == null) {
                correspondingDomain = new Domain(currentDomainName);
            }
            final AttributesSelectComboBox attributeComboBox = selectionBean.getSelectionPanel().getAttributeComboBox();
            attributeComboBox.setAttributes(correspondingDomain, selectedDeviceClass);
        }
    }

    /**
     * @param selected
     * @throws EntityException
     */
    private void newDomainSelected(final String selected) throws EntityException {
        if (selectionBean != null && selectionBean.getSelectionPanel() != null) {
            final AttributesSelectComboBox attributeComboBox = selectionBean.getSelectionPanel().getAttributeComboBox();
            attributeComboBox.reset();
            final AttributesSelectComboBox deviceClassComboBox = selectionBean.getSelectionPanel()
                    .getDeviceClassComboBox();
            final Domain domain = new Domain(selected);
            deviceClassComboBox.setDeviceClasses(domain);
        }
    }

}
