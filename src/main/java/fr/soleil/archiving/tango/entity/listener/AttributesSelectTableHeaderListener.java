package fr.soleil.archiving.tango.entity.listener;

import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.table.JTableHeader;

import fr.soleil.archiving.tango.entity.model.AttributesSelectTableModel;
import fr.soleil.archiving.tango.entity.ui.AttributesSelectTable;

/**
 * Listens to double clicks on the {@link AttributesSelectTable} header.
 * Responds by sorting the clicked column.
 * <ul>
 * <li>Checks the click is not a single click, if it is does nothing</li>
 * <li>Gets the index of the clicked column</li>
 * <li>Sorts the {@link AttributesSelectTableModel} at this column index</li>
 * </ul>
 */
public class AttributesSelectTableHeaderListener extends MouseAdapter {

    /**
     * Default constructor
     */
    public AttributesSelectTableHeaderListener() {
        super();
    }

    @Override
    public void mousePressed(MouseEvent event) {
        if (event != null) {
            if ((event.getClickCount() > 1) && (event.getSource() instanceof JTableHeader)) {
                Point point = event.getPoint();
                JTableHeader header = (JTableHeader) event.getSource();
                if ((header.getTable() != null) && (header.getTable().getModel() instanceof AttributesSelectTableModel)) {
                    ((AttributesSelectTableModel) header.getTable().getModel()).sort(header.columnAtPoint(point));
                }
            }
        }
    }

}
