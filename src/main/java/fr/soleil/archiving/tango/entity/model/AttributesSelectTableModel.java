package fr.soleil.archiving.tango.entity.model;

import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeMap;

import javax.swing.table.DefaultTableModel;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.ui.comparator.AttributeComparator;

/**
 * The table model used by ContextListTable, this model lists the current list
 * of contexts. Its rows are ContextData objects. A singleton class.
 */
public class AttributesSelectTableModel extends DefaultTableModel {

    private static final long serialVersionUID = -985170870002207670L;

    private Attribute[] rows;
    private final TreeMap<String, Attribute> htAttr;
    private int idSort = AttributeComparator.NO_SORT;
    private final AttributeTableSelectionBean selectionBean;

    /**
     * Initializes the columns titles, and adds a ContextTableModelListener to
     * itself.
     */
    public AttributesSelectTableModel(AttributeTableSelectionBean selectionBean) {
        super();
        htAttr = new TreeMap<String, Attribute>(Collator.getInstance());
        this.selectionBean = selectionBean;
    }

    /**
     * Removes all rows and refreshes the model.
     */
    public void reset() {
        if (this.getRowCount() != 0) {
            int firstRemoved = 0;
            int lastRemoved = this.getRowCount() - 1;

            this.rows = null;
            this.htAttr.clear();

            this.fireTableRowsDeleted(firstRemoved, lastRemoved);
        }
    }

    public Attribute getAttributeAtRow(int rowIndex) {
        if (rows != null) {
            return rows[rowIndex];
        } else {
            return null;
        }
    }

    /**
     * Removes all rows which indexes are found in <code>indexesToRemove</code>.
     * 
     * @param indexesToRemove
     *            The list of rows to remove
     */
    public Attribute[] removeRows(int[] indexesToRemove) {
        if (indexesToRemove != null) {
            int numberOfLinesToRemove = indexesToRemove.length;
            Attribute[] newRows = new Attribute[rows.length - numberOfLinesToRemove];

            ArrayList<String> idsToRemoveList = new ArrayList<String>(numberOfLinesToRemove);
            ArrayList<Attribute> removed = new ArrayList<Attribute>();
            for (int i = 0; i < numberOfLinesToRemove; i++) {
                if ((indexesToRemove[i] > -1) && (getAttributeAtRow(indexesToRemove[i]) != null)) {
                    idsToRemoveList.add(getAttributeAtRow(indexesToRemove[i]).getCompleteName());
                }
            }

            int j = 0;
            for (int i = 0; i < rows.length; i++) {
                String idOfCurrentLine = this.getAttributeAtRow(i).getCompleteName();
                if (idsToRemoveList.contains(idOfCurrentLine)) {
                    removed.add(htAttr.get(idOfCurrentLine));
                    htAttr.remove(idOfCurrentLine);
                } else {
                    newRows[j] = rows[i];
                    j++;
                }
            }

            rows = newRows;
            this.fireTableDataChanged();
            return removed.toArray(new Attribute[removed.size()]);
        }
        return null;
    }

    @Override
    public int getColumnCount() {
        return 4;
    }

    @Override
    public int getRowCount() {
        if (rows == null) {
            return 0;
        }

        return rows.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = null;

        switch (columnIndex) {
            case 0:
                value = rows[rowIndex].getDomainName();
                break;

            case 1:
                value = rows[rowIndex].getDeviceName();
                break;

            case 2:
                value = rows[rowIndex].getName();
                break;

            case 3:
                value = new Boolean(rows[rowIndex].isSelected());
                break;

            default:
                return null;
        }

        return value;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (rowIndex >= this.getRowCount()) {
            return;
        }

        switch (columnIndex) {
            case 3:
                Boolean _isSelected = (Boolean) aValue;
                rows[rowIndex].setSelected(_isSelected.booleanValue());
                break;

            default:
                // do nothing, only write values can be edited
        }
    }

    @Override
    public String getColumnName(int columnIndex) {
        if (selectionBean == null) {
            return super.getColumnName(columnIndex);
        } else {
            switch (columnIndex) {
                case 0:
                    return selectionBean.getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.Domain");
                case 1:
                    return selectionBean.getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.Device");
                case 2:
                    return selectionBean.getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.Attribute");
                case 3:
                    return " ";
                default:
                    return super.getColumnName(columnIndex);
            }
        }
    }

    /**
     * Adds a line at the end of the current model for the specified Attribute,
     * and refreshes the list.
     * 
     * @param newAttribute
     *            The Attribute to add to the current list
     */
    public void addAttribute(Attribute newAttribute) {
        if (newAttribute != null) {
            String newACAttributeName = newAttribute.getCompleteName();
            if (!htAttr.containsKey(newACAttributeName)) {
                Attribute[] before = this.rows;
                int rowIndex = this.getRowCount();
                rows = new Attribute[rowIndex + 1];

                for (int i = 0; i < rowIndex; i++) {
                    rows[i] = before[i];
                }

                rows[rowIndex] = newAttribute;
                newAttribute.setNew(true);
                htAttr.put(newACAttributeName, newAttribute);
                fireTableRowsInserted(rowIndex, rowIndex);
            }
        }
    }

    /**
     * @return Returns the rows.
     */
    public Attribute[] getRows() {
        return rows;
    }

    /**
     * @param rows
     *            The rows to set.
     */
    public void setRows(Attribute[] rows) {
        this.rows = rows;
        htAttr.clear();
        if ((rows != null) && (rows.length > 0)) {
            for (int i = 0; i < rows.length; i++) {
                htAttr.put(rows[i].getCompleteName(), rows[i]);
            }
        }
        fireTableDataChanged();
    }

    /**
     * @param selectedRows
     */
    public void reverseSelectionForRows(int[] selectedRows) {
        if (selectedRows != null) {
            for (int i = 0; i < selectedRows.length; i++) {
                if (getAttributeAtRow(selectedRows[i]) != null) {
                    getAttributeAtRow(selectedRows[i]).reverseSelection();
                    fireTableRowsUpdated(selectedRows[i], selectedRows[i]);
                }
            }
        }
    }

    /**
     * @param b
     */
    public void selectAllOrNone(boolean b) {
        if (rows != null) {
            for (int i = 0; i < rows.length; i++) {
                if (getAttributeAtRow(i) != null) {
                    getAttributeAtRow(i).setSelected(b);
                }
            }
            this.fireTableRowsUpdated(0, this.getRowCount() - 1);
        }

    }

    /**
     * Sorts the table's lines relative to the specified column. If the the
     * table is already sorted relative to this column, reverses the sort.
     * 
     * @param clickedColumnIndex
     *            The index of the column to sort the lines by
     */
    public void sort(int clickedColumnIndex) {
        switch (clickedColumnIndex) {
            case 0:
                sortByColumn(AttributeComparator.COMPARE_DEVICE_CLASS);
                break;

            case 1:
                sortByColumn(AttributeComparator.COMPARE_DEVICE);
                break;

            case 2:
                sortByColumn(AttributeComparator.COMPARE_NAME);
                break;
        }
    }

    /**
     * Sorts the table's lines relative to the specified field. If the the table
     * is already sorted relative to this column, reverses the sort.
     * 
     * @param compareCase
     *            The type of field to sort the lines by
     */
    private void sortByColumn(int compareCase) {
        int newSortType = AttributeComparator.getNewSortType(this.idSort);

        ArrayList<Attribute> al = new ArrayList<Attribute>();
        for (int i = 0; i < rows.length; i++) {
            al.add(this.getAttributeAtRow(i));
        }

        Collections.sort(al, new AttributeComparator(compareCase));
        if (newSortType == AttributeComparator.SORT_DOWN) {
            Collections.reverse(al);
        }

        Attribute[] newRows = al.toArray(new Attribute[al.size()]);

        this.rows = newRows;
        this.fireTableDataChanged();

        al = null;
        this.idSort = newSortType;
    }

    public Attribute[] removeNotSelectedRows() {
        ArrayList<Integer> listOfIndexesToRemove = new ArrayList<Integer>();
        for (int i = 0; i < this.getRowCount(); i++) {
            Attribute attr = this.getAttributeAtRow(i);
            if (!attr.isSelected()) {
                listOfIndexesToRemove.add(new Integer(i));
            }
            attr = null;
        }
        Integer[] tempIntegers = listOfIndexesToRemove.toArray(new Integer[listOfIndexesToRemove.size()]);
        int[] indexesToRemove = new int[tempIntegers.length];
        for (int i = 0; i < tempIntegers.length; i++) {
            if (tempIntegers[i] == null) {
                indexesToRemove[i] = -1;
            } else {
                indexesToRemove[i] = tempIntegers[i].intValue();
            }
        }
        listOfIndexesToRemove.clear();
        listOfIndexesToRemove = null;
        tempIntegers = null;
        return removeRows(indexesToRemove);
    }

    public Attribute[] getSelectedRows() {
        ArrayList<Attribute> selected = new ArrayList<Attribute>();
        for (int i = 0; i < this.getRowCount(); i++) {
            Attribute attr = this.getAttributeAtRow(i);
            if (attr.isSelected()) {
                selected.add(attr);
            }
            attr = null;
        }
        return selected.toArray(new Attribute[selected.size()]);
    }

}
