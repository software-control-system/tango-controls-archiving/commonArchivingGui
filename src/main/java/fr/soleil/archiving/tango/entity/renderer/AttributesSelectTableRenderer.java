package fr.soleil.archiving.tango.entity.renderer;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.model.AttributesSelectTableModel;
import fr.soleil.archiving.tango.entity.ui.AttributesSelectTable;

/**
 * A cell renderer used for {@link AttributesSelectTable}. It paints cells
 * containing "Not applicable" values in grey, and modified values in red. In
 * all other cases, it paints the cell with the default component.
 */
public class AttributesSelectTableRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 169271220272359833L;

    private final static Color newColorNotSelected = new Color(255, 220, 220);
    private final static Color newColorSelected = new Color(255, 150, 150);
    private final TableCellRenderer headerRenderer;

    public AttributesSelectTableRenderer() {
        super();
        headerRenderer = new JTable().getTableHeader().getDefaultRenderer();
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus,
            int row, int column) {
        if (row == -1) {
            return headerRenderer.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        } else {
            if ((column == 3) && (value instanceof Boolean)) {
                JCheckBox ret = new JCheckBox();
                ret.setSelected(((Boolean) value).booleanValue());
                return ret;
            } else {
                Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
                // use new JLabel to be sure to return a clean component
                JLabel ret = new JLabel();
                ret.setText(((JLabel) comp).getText());
                ret.setToolTipText(((JLabel) comp).getToolTipText());
                ret.setBackground(comp.getBackground());
                ret.setForeground(comp.getForeground());
                ret.setFont(comp.getFont());
                ret.setBorder(((JLabel) comp).getBorder());
                ret.setOpaque(((JLabel) comp).isOpaque());
                if (table.getModel() instanceof AttributesSelectTableModel) {
                    AttributesSelectTableModel model = (AttributesSelectTableModel) table.getModel();
                    Attribute attr = model.getAttributeAtRow(row);

                    if ((attr != null) && (attr.isNew())) {
                        ret.setBackground(isSelected ? newColorSelected : newColorNotSelected);
                    }
                    attr = null;
                    model = null;
                }
                comp = null;
                return ret;
            }
        }
    }
}
