package fr.soleil.archiving.tango.entity.ui;

import java.awt.Dimension;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JComboBox;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.Entity;
import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.datasource.ITangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.exception.EntityException;
import fr.soleil.archiving.tango.entity.listener.AttributesSelectItemListener;

public class AttributesSelectComboBox extends JComboBox<String> {

    private static final long serialVersionUID = 8129620970044491221L;

    public static final String NO_SELECTION = "---";

    public static final int DOMAIN_TYPE = 0;
    public static final int DEVICE_CLASS_TYPE = 1;
    public static final int ATTRIBUTE_TYPE = 2;

    private final int type;
    private Map<String, Entity> elements;
    private final AttributeTableSelectionBean selectionBean;

    /**
     * @param type
     * @throws IllegalStateException
     */
    public AttributesSelectComboBox(final int _type, final AttributeTableSelectionBean selectionBean)
            throws IllegalStateException {
        super();
        this.selectionBean = selectionBean;
        this.type = _type;
        this.reset();

        switch (_type) {
            case DOMAIN_TYPE:
                this.setDomains();
                break;

            case DEVICE_CLASS_TYPE:

                break;

            case ATTRIBUTE_TYPE:

                break;

            default:
                throw new IllegalStateException("Expected either of " + DOMAIN_TYPE + "," + DEVICE_CLASS_TYPE + ","
                        + ATTRIBUTE_TYPE + " as a parameter. Received " + type + " instead.");
        }

        final Dimension maxDimension = new Dimension(Integer.MAX_VALUE, 20);
        this.setMaximumSize(maxDimension);

        if (selectionBean != null) {
            this.addItemListener(new AttributesSelectItemListener(selectionBean));
        }
    }

    public void setDomains() {
        if (this.type != AttributesSelectComboBox.DOMAIN_TYPE) {
            throw new IllegalStateException();
        }

        this.reset();
        if (selectionBean != null && selectionBean.getTangoManager() != null) {
            ITangoEntitiesSelectionManager manager = selectionBean.getTangoManager();
            Domain[] domains = null;
            try {
                domains = manager.loadDomains();
            } catch (final EntityException e) {
                e.printStackTrace();
            }
            if (domains != null) {
                for (int i = 0; i < domains.length; i++) {
                    super.addItem(domains[i].getName());
                    this.elements.put(domains[i].getName(), domains[i]);
                }
            }
            domains = null;
            manager = null;
        }
    }

    public void setDeviceClasses(final Domain domain) throws EntityException {
        if (this.type != AttributesSelectComboBox.DEVICE_CLASS_TYPE) {
            throw new IllegalStateException();
        }
        this.reset();
        if (selectionBean != null && selectionBean.getTangoManager() != null) {
            ITangoEntitiesSelectionManager manager = selectionBean.getTangoManager();
            DeviceClass[] deviceClasses = manager.loadDeviceClasses(domain);
            if (deviceClasses != null) {
                for (int i = 0; i < deviceClasses.length; i++) {
                    super.addItem(deviceClasses[i].getName());
                    this.elements.put(deviceClasses[i].getName(), deviceClasses[i]);
                }
            }
            deviceClasses = null;
            manager = null;
        }
    }

    public void setAttributes(final Domain domain, final DeviceClass deviceClass) {
        if (this.type != AttributesSelectComboBox.ATTRIBUTE_TYPE) {
            throw new IllegalStateException();
        }
        this.reset();
        if (selectionBean != null && selectionBean.getTangoManager() != null) {
            ITangoEntitiesSelectionManager manager = selectionBean.getTangoManager();
            Attribute[] attributes = null;
            try {
                attributes = manager.loadAttributes(domain, deviceClass);
            } catch (final EntityException e) {
                e.printStackTrace();
            }
            if (attributes != null) {
                for (int i = 0; i < attributes.length; i++) {
                    super.addItem(attributes[i].getName());
                    if (attributes[i] == null || attributes[i].getName() == null) {
                        continue;
                    }
                    this.elements.put(attributes[i].getName(), attributes[i]);
                }
            }
            attributes = null;
            manager = null;
        }
    }

    /**
     * @return Returns the type.
     */
    public int getType() {
        return type;
    }

    /**
     *
     */
    public void reset() {
        super.removeAllItems();
        super.addItem(NO_SELECTION);

        this.elements = new HashMap<String, Entity>();
    }

    public Attribute getSelectedAttribute() {
        if (this.type != AttributesSelectComboBox.ATTRIBUTE_TYPE) {
            throw new IllegalStateException();
        }

        final String currentAttributeName = (String) super.getSelectedItem();
        if (currentAttributeName == null || currentAttributeName.equals(AttributesSelectComboBox.NO_SELECTION)) {
            return null;
        }

        final Attribute ret = (Attribute) this.elements.get(currentAttributeName);
        return ret;
    }

    /**
     * @return
     */
    public DeviceClass getSelectedDeviceClass() {
        if (this.type != AttributesSelectComboBox.DEVICE_CLASS_TYPE) {
            throw new IllegalStateException();
        }

        final String currentDeviceClassName = (String) super.getSelectedItem();
        if (currentDeviceClassName == null || currentDeviceClassName.equals(AttributesSelectComboBox.NO_SELECTION)) {
            return null;
        }

        final DeviceClass ret = (DeviceClass) this.elements.get(currentDeviceClassName);
        return ret;
    }

    /**
     * @return
     */
    public Domain getSelectedDomain() {
        if (this.type != AttributesSelectComboBox.DOMAIN_TYPE) {
            throw new IllegalStateException();
        }

        final String currentDomainName = (String) super.getSelectedItem();
        if (currentDomainName == null || currentDomainName.equals(AttributesSelectComboBox.NO_SELECTION)) {
            return null;
        }

        final Domain ret = (Domain) this.elements.get(currentDomainName);
        return ret;
    }

    /**
     * @param domains
     * @throws EntityException
     */
    public void setDeviceClasses(final Domain[] domains) throws EntityException {
        if (this.type != AttributesSelectComboBox.DEVICE_CLASS_TYPE) {
            throw new IllegalStateException();
        }
        this.reset();
        if (selectionBean != null && selectionBean.getTangoManager() != null) {
            ITangoEntitiesSelectionManager manager = selectionBean.getTangoManager();
            DeviceClass[] deviceClasses = manager.loadDeviceClasses(domains);
            if (deviceClasses != null) {
                for (int i = 0; i < deviceClasses.length; i++) {
                    super.addItem(deviceClasses[i].getName());
                    this.elements.put(deviceClasses[i].getName(), deviceClasses[i]);
                }
            }
            deviceClasses = null;
            manager = null;
        }
    }

    public void setDeviceClasses(final DeviceClass[] deviceClasses) throws EntityException {
        if (this.type != AttributesSelectComboBox.DEVICE_CLASS_TYPE) {
            throw new IllegalStateException();
        }
        this.reset();
        if (deviceClasses != null) {
            for (int i = 0; i < deviceClasses.length; i++) {
                super.addItem(deviceClasses[i].getName());
                this.elements.put(deviceClasses[i].getName(), deviceClasses[i]);
            }
        }
    }

    public void selectNeutralElement() {
        this.setSelectedItem(NO_SELECTION);
    }
}
