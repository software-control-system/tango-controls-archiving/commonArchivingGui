package fr.soleil.archiving.tango.entity.ui;

import javax.swing.JTable;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;

import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.editor.AttributesSelectTableEditor;
import fr.soleil.archiving.tango.entity.listener.AttributesSelectTableHeaderListener;
import fr.soleil.archiving.tango.entity.model.AttributesSelectTableModel;
import fr.soleil.archiving.tango.entity.renderer.AttributesSelectTableRenderer;

/**
 * A class containing the current list of contexts. The table's cells are not
 * editable. A ContextTableListener is added that listens to line selection
 * events, and a ContextTableHeaderListener is added that listens to column
 * double-clicks to sort them.
 */
public class AttributesSelectTable extends JTable {

    private static final long serialVersionUID = -1068287851424642295L;

    private final AttributesSelectTableModel defaultModel;

    /**
     * Default constructor.
     * <ul>
     * <li>Instantiates its table model</li>
     * <li>Adds a selection listener on its table body (ContextTableListener)</li>
     * <li>Adds a sort request listener on its table header (ContextTableHeaderListener)</li>
     * <li>Sets its columns sizes and row height</li>
     * <li>Disables the columns auto resize mode</li>
     * </ul>
     */
    public AttributesSelectTable(AttributeTableSelectionBean attributeTableSelectionBean) {
        super(new AttributesSelectTableModel(attributeTableSelectionBean));

        defaultModel = new AttributesSelectTableModel(null);

        this.setDefaultRenderer(Object.class, new AttributesSelectTableRenderer());
        this.setDefaultEditor(Object.class, new AttributesSelectTableEditor());

        JTableHeader header = this.getTableHeader();
        header.addMouseListener(new AttributesSelectTableHeaderListener());

        this.setRowHeight(20);
        this.getColumn(" ").setMaxWidth(25);
        this.getColumn(" ").setMinWidth(25);
    }

    @Override
    public boolean isCellEditable(int row, int column) {
        if (column == 3) {
            return true;
        }
        return false;
    }

    public void reverseSelection() {
        int[] selectedRows = this.getSelectedRows();
        AttributesSelectTableModel model = this.getModel();
        model.reverseSelectionForRows(selectedRows);
    }

    public void selectAllOrNone(boolean b) {
        AttributesSelectTableModel model = this.getModel();
        model.selectAllOrNone(b);
    }

    public void applyChange() {
        AttributesSelectTableModel model = this.getModel();
        AttributesSelectTableEditor editor = (AttributesSelectTableEditor) this.getCellEditor();

        if (this.isEditing()) {
            model.setValueAt(editor.getCellEditorValue(), this.getEditingRow(), this.getEditingColumn());
            editor.stopCellEditing();
        }
    }

    @Override
    public AttributesSelectTableModel getModel() {
        return (AttributesSelectTableModel) super.getModel();
    }

    @Override
    public void setModel(TableModel dataModel) {
        if (dataModel == null) {
            defaultModel.reset();
            super.setModel(defaultModel);
        } else if (dataModel instanceof AttributesSelectTableModel) {
            super.setModel(dataModel);
        }
    }

}
