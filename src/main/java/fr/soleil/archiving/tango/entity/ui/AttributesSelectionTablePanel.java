package fr.soleil.archiving.tango.entity.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

import fr.soleil.archiving.tango.entity.IPreBufferingEventListener;
import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.ui.action.AddSelectedAttributeToTableAction;
import fr.soleil.archiving.tango.entity.ui.action.DomainsFilterinComboBoxAction;
import fr.soleil.archiving.tango.entity.ui.action.TableSelectionAction;

public class AttributesSelectionTablePanel extends JPanel implements IPreBufferingEventListener {

    private static final long serialVersionUID = -4798075011062020473L;

    private static final String OPEN_PARENTHESIS = "(";
    private static final String BUFFERING_STATUS = "fr.soleil.archiving.tango.entity.AttributesSelection.BufferingStatus";
    private static final String TITLE_SEPARATOR = ": ";
    private static final String BUFFERING_STATUS_CANCELED = "fr.soleil.archiving.tango.entity.AttributesSelection.BufferingStatus.Canceled";
    private static final String SPACE = " ";
    private static final String DOMAINS = "fr.soleil.archiving.tango.entity.AttributesSelection.Domains";
    private static final String DOMAIN = "fr.soleil.archiving.tango.entity.AttributesSelection.Domain";
    private static final String DEVICE_CLASS = "fr.soleil.archiving.tango.entity.AttributesSelection.DeviceClass";
    private static final String ATTRIBUTE = "fr.soleil.archiving.tango.entity.AttributesSelection.Attribute";
    private static final String ATTRIBUTE_ADD = "fr.soleil.archiving.tango.entity.AttributesSelection.Attribute.Add";
    private static final String DOMAINS_REG_EXP = "fr.soleil.archiving.tango.entity.AttributesSelection.Domains.RegExp";
    private static final String SELECTION = "fr.soleil.archiving.tango.entity.AttributesSelection.Selection";
    private static final String SELECTION_REVERSE_TEXT = "fr.soleil.archiving.tango.entity.AttributesSelection.Selection.Reverse.Text";
    private static final String SELECTION_REVERSE_TOOLTIP_TEXT = "fr.soleil.archiving.tango.entity.AttributesSelection.Selection.Reverse.TooltipText";
    private static final String SELECTION_NONE_TEXT = "fr.soleil.archiving.tango.entity.AttributesSelection.Selection.None.Text";
    private static final String SELECTION_NONE_TOOLTIP_TEXT = "fr.soleil.archiving.tango.entity.AttributesSelection.Selection.None.TooltipText";
    private static final String SELECTION_ALL_TEXT = "fr.soleil.archiving.tango.entity.AttributesSelection.Selection.All.Text";
    private static final String SELECTION_ALL_TOOLTIP_TEXT = "fr.soleil.archiving.tango.entity.AttributesSelection.Selection.All.TooltipText";

    // ---------LEFT HALF
    private JPanel topHalf;
    private JPanel selectionPanel;

    private JLabel domainsLabel;
    private JTextField domainsField;
    private JButton domainsButton;
    private JLabel domainLabel;
    private JLabel deviceClassLabel;
    private JLabel attributeLabel;
    private JButton okButton;
    private JLabel selectionLabel;
    private JButton selectReverseButton;
    private JButton selectAllButton;
    private JButton selectNoneButton;

    private JLabel bufferingStatusLabel;

    private AttributesSelectComboBox domainComboBox;
    private AttributesSelectComboBox deviceClassComboBox;
    private AttributesSelectComboBox attributeComboBox;
    // ---------LEFT HALF

    // ---------RIGHT HALF
    private AttributesSelectTable attributesSelectTable;
    private JScrollPane scrollpane;
    private int step;
    private int totalSteps;

    private final AttributeTableSelectionBean selectionBean;

    public AttributesSelectionTablePanel(AttributeTableSelectionBean selectionBean) {
        super(new GridBagLayout());
        step = 0;
        totalSteps = 0;
        this.selectionBean = selectionBean;
        this.initComponents();
        this.addComponents();
    }

    private void addComponents() {
        addComponentsTop();
        GridBagConstraints topConstraints = new GridBagConstraints();
        topConstraints.fill = GridBagConstraints.HORIZONTAL;
        topConstraints.gridx = 0;
        topConstraints.gridy = 0;
        topConstraints.weightx = 1;
        topConstraints.weighty = 0;
        topConstraints.insets = new Insets(10, 0, 0, 0);
        add(topHalf, topConstraints);
        GridBagConstraints scrollpaneConstraints = new GridBagConstraints();
        scrollpaneConstraints.fill = GridBagConstraints.BOTH;
        scrollpaneConstraints.gridx = 0;
        scrollpaneConstraints.gridy = 1;
        scrollpaneConstraints.weightx = 1;
        scrollpaneConstraints.weighty = 1;
        scrollpaneConstraints.insets = new Insets(10, 0, 10, 0);
        add(scrollpane, scrollpaneConstraints);
    }

    /**
     * 
     */
    private void addComponentsTop() {
        topHalf = new JPanel(new GridBagLayout());

        Insets firstInsets = new Insets(0, 6, 0, 0);
        Insets defaultInsets = new Insets(6, 6, 0, 0);

        // START LINE 0
        GridBagConstraints bufferingStatusLabelConstraints = new GridBagConstraints();
        bufferingStatusLabelConstraints.fill = GridBagConstraints.HORIZONTAL;
        bufferingStatusLabelConstraints.gridx = 0;
        bufferingStatusLabelConstraints.gridy = 0;
        bufferingStatusLabelConstraints.weightx = 1;
        bufferingStatusLabelConstraints.weighty = 0;
        bufferingStatusLabelConstraints.gridwidth = 2;
        bufferingStatusLabelConstraints.insets = new Insets(0, 0, 0, 0);
        bufferingStatusLabelConstraints.anchor = GridBagConstraints.EAST;
        topHalf.add(bufferingStatusLabel, bufferingStatusLabelConstraints);
        // END LINE 0

        // START LINE 1
        GridBagConstraints domainsLabelConstraints = new GridBagConstraints();
        domainsLabelConstraints.fill = GridBagConstraints.BOTH;
        domainsLabelConstraints.gridx = 0;
        domainsLabelConstraints.gridy = 1;
        domainsLabelConstraints.weightx = 0;
        domainsLabelConstraints.weighty = 0;
        domainsLabelConstraints.insets = firstInsets;
        topHalf.add(domainsLabel, domainsLabelConstraints);
        GridBagConstraints domainsFieldConstraints = new GridBagConstraints();
        domainsFieldConstraints.fill = GridBagConstraints.BOTH;
        domainsFieldConstraints.gridx = 1;
        domainsFieldConstraints.gridy = 1;
        domainsFieldConstraints.weightx = 0;
        domainsFieldConstraints.weighty = 1;
        domainsFieldConstraints.insets = firstInsets;
        topHalf.add(domainsField, domainsFieldConstraints);
        GridBagConstraints domainsButtonConstraints = new GridBagConstraints();
        domainsButtonConstraints.fill = GridBagConstraints.BOTH;
        domainsButtonConstraints.gridx = 2;
        domainsButtonConstraints.gridy = 1;
        domainsButtonConstraints.weightx = 0;
        domainsButtonConstraints.weighty = 0;
        domainsButtonConstraints.insets = new Insets(0, defaultInsets.left, 0, defaultInsets.left);
        topHalf.add(domainsButton, domainsButtonConstraints);
        // END LINE 1

        // START LINE 2
        GridBagConstraints domainLabelConstraints = new GridBagConstraints();
        domainLabelConstraints.fill = GridBagConstraints.BOTH;
        domainLabelConstraints.gridx = 0;
        domainLabelConstraints.gridy = 2;
        domainLabelConstraints.weightx = 0;
        domainLabelConstraints.weighty = 0;
        domainLabelConstraints.insets = defaultInsets;
        topHalf.add(domainLabel, domainLabelConstraints);
        GridBagConstraints domainComboBoxConstraints = new GridBagConstraints();
        domainComboBoxConstraints.fill = GridBagConstraints.BOTH;
        domainComboBoxConstraints.gridx = 1;
        domainComboBoxConstraints.gridy = 2;
        domainComboBoxConstraints.weightx = 0;
        domainComboBoxConstraints.weighty = 1;
        domainComboBoxConstraints.insets = defaultInsets;
        topHalf.add(domainComboBox, domainComboBoxConstraints);
        // END LINE 2

        // START LINE 3
        GridBagConstraints deviceClassLabelConstraints = new GridBagConstraints();
        deviceClassLabelConstraints.fill = GridBagConstraints.BOTH;
        deviceClassLabelConstraints.gridx = 0;
        deviceClassLabelConstraints.gridy = 3;
        deviceClassLabelConstraints.weightx = 0;
        deviceClassLabelConstraints.weighty = 0;
        deviceClassLabelConstraints.insets = defaultInsets;
        topHalf.add(deviceClassLabel, deviceClassLabelConstraints);
        GridBagConstraints deviceClassComboBoxConstraints = new GridBagConstraints();
        deviceClassComboBoxConstraints.fill = GridBagConstraints.BOTH;
        deviceClassComboBoxConstraints.gridx = 1;
        deviceClassComboBoxConstraints.gridy = 3;
        deviceClassComboBoxConstraints.weightx = 0;
        deviceClassComboBoxConstraints.weighty = 1;
        deviceClassComboBoxConstraints.insets = defaultInsets;
        topHalf.add(deviceClassComboBox, deviceClassComboBoxConstraints);
        // END LINE 3

        // START LINE 4
        GridBagConstraints attributeLabelConstraints = new GridBagConstraints();
        attributeLabelConstraints.fill = GridBagConstraints.BOTH;
        attributeLabelConstraints.gridx = 0;
        attributeLabelConstraints.gridy = 4;
        attributeLabelConstraints.weightx = 0;
        attributeLabelConstraints.weighty = 0;
        attributeLabelConstraints.insets = defaultInsets;
        topHalf.add(attributeLabel, attributeLabelConstraints);
        GridBagConstraints attributeComboBoxConstraints = new GridBagConstraints();
        attributeComboBoxConstraints.fill = GridBagConstraints.BOTH;
        attributeComboBoxConstraints.gridx = 1;
        attributeComboBoxConstraints.gridy = 4;
        attributeComboBoxConstraints.weightx = 0;
        attributeComboBoxConstraints.weighty = 1;
        attributeComboBoxConstraints.insets = defaultInsets;
        topHalf.add(attributeComboBox, attributeComboBoxConstraints);
        // END LINE 4

        // START LINE 5
        GridBagConstraints okButtonConstraints = new GridBagConstraints();
        okButtonConstraints.fill = GridBagConstraints.BOTH;
        okButtonConstraints.gridx = 1;
        okButtonConstraints.gridy = 5;
        okButtonConstraints.weightx = 0;
        okButtonConstraints.weighty = 1;
        okButtonConstraints.insets = new Insets(defaultInsets.top, defaultInsets.left, 10, defaultInsets.right);
        topHalf.add(okButton, okButtonConstraints);
        // END LINE 5

        // START LINE 6
        GridBagConstraints selectionLabelConstraints = new GridBagConstraints();
        selectionLabelConstraints.fill = GridBagConstraints.BOTH;
        selectionLabelConstraints.gridx = 0;
        selectionLabelConstraints.gridy = 6;
        selectionLabelConstraints.weightx = 0;
        selectionLabelConstraints.weighty = 0;
        selectionLabelConstraints.insets = defaultInsets;
        topHalf.add(selectionLabel, selectionLabelConstraints);
        GridBagConstraints selectionPanelConstraints = new GridBagConstraints();
        selectionPanelConstraints.fill = GridBagConstraints.BOTH;
        selectionPanelConstraints.gridx = 1;
        selectionPanelConstraints.gridy = 6;
        selectionPanelConstraints.weightx = 0;
        selectionPanelConstraints.weighty = 1;
        selectionPanelConstraints.insets = defaultInsets;
        topHalf.add(selectionPanel, selectionPanelConstraints);
        // END LINE 6
    }

    /**
     * 19 juil. 2005
     */
    private void initComponents() {
        initComponentsTop();
        initComponentsBottom();
    }

    private void initComponentsBottom() {
        attributesSelectTable = new AttributesSelectTable(selectionBean);

        scrollpane = new JScrollPane(attributesSelectTable);
        scrollpane.setMinimumSize(new Dimension(50, 100));
        scrollpane.setPreferredSize(new Dimension(50, 100));
        scrollpane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
    }

    private void initComponentsTop() {
        topHalf = new JPanel();

        bufferingStatusLabel = new JLabel(computeBufferingStatusText(), JLabel.RIGHT);
        Font bufferingStatusLabelFont = new Font(null, Font.ITALIC, 10);
        bufferingStatusLabel.setFont(bufferingStatusLabelFont);
        bufferingStatusLabel.setForeground(Color.RED);

        domainsLabel = new JLabel(computeDomainsLabelText(), JLabel.RIGHT);
        domainLabel = new JLabel(computeDomainLabelText(), JLabel.RIGHT);
        deviceClassLabel = new JLabel(computeDeviceClassLabelText(), JLabel.RIGHT);
        attributeLabel = new JLabel(computeAttributeLabelText(), JLabel.RIGHT);
        selectionLabel = new JLabel(computeSelectionLabelText(), JLabel.RIGHT);

        okButton = new JButton(new AddSelectedAttributeToTableAction(computeOkButtonText(), selectionBean));
        domainsField = new JTextField();
        domainsButton = new JButton(new DomainsFilterinComboBoxAction(computeDomainsButtonText(), selectionBean));
        selectReverseButton = new JButton(new TableSelectionAction(computeSelectReverseButtonText(),
                TableSelectionAction.SELECT_REVERSE_TYPE, selectionBean));
        selectReverseButton.setToolTipText(computeSelectReverseButtonTooltipText());
        selectNoneButton = new JButton(new TableSelectionAction(computeSelectNoneButtonText(),
                TableSelectionAction.SELECT_NONE_TYPE, selectionBean));
        selectNoneButton.setToolTipText(computeSelectNoneButtonTooltipText());
        selectAllButton = new JButton(new TableSelectionAction(computeSelectAllButtonText(),
                TableSelectionAction.SELECT_ALL_TYPE, selectionBean));
        selectAllButton.setToolTipText(computeSelectAllButtonTooltipText());

        selectionPanel = new JPanel(new GridBagLayout());
        double weightx = 1.0d / 3.0d;
        Insets gap = new Insets(0, 0, 0, 5);
        GridBagConstraints selectReverseButtonConstraints = new GridBagConstraints();
        selectReverseButtonConstraints.fill = GridBagConstraints.BOTH;
        selectReverseButtonConstraints.gridx = 0;
        selectReverseButtonConstraints.gridy = 0;
        selectReverseButtonConstraints.weightx = weightx;
        selectReverseButtonConstraints.weighty = 1;
        selectReverseButtonConstraints.insets = gap;
        selectionPanel.add(selectReverseButton, selectReverseButtonConstraints);
        GridBagConstraints selectAllButtonConstraints = new GridBagConstraints();
        selectAllButtonConstraints.fill = GridBagConstraints.BOTH;
        selectAllButtonConstraints.gridx = 1;
        selectAllButtonConstraints.gridy = 0;
        selectAllButtonConstraints.weightx = weightx;
        selectAllButtonConstraints.weighty = 1;
        selectAllButtonConstraints.insets = gap;
        selectionPanel.add(selectAllButton, selectAllButtonConstraints);
        GridBagConstraints selectNoneButtonConstraints = new GridBagConstraints();
        selectNoneButtonConstraints.fill = GridBagConstraints.BOTH;
        selectNoneButtonConstraints.gridx = 2;
        selectNoneButtonConstraints.gridy = 0;
        selectNoneButtonConstraints.weightx = weightx;
        selectNoneButtonConstraints.weighty = 1;
        selectionPanel.add(selectNoneButton, selectNoneButtonConstraints);
        gap = null;

        domainComboBox = new AttributesSelectComboBox(AttributesSelectComboBox.DOMAIN_TYPE, selectionBean);
        deviceClassComboBox = new AttributesSelectComboBox(AttributesSelectComboBox.DEVICE_CLASS_TYPE, selectionBean);
        attributeComboBox = new AttributesSelectComboBox(AttributesSelectComboBox.ATTRIBUTE_TYPE, selectionBean);
    }

    private String computeBufferingStatusText() {
        StringBuilder buffer = new StringBuilder(OPEN_PARENTHESIS);
        if (selectionBean == null) {
            buffer.append(BUFFERING_STATUS);
            buffer.append(TITLE_SEPARATOR);
        } else {
            buffer.append(selectionBean.getMessage(BUFFERING_STATUS));
            buffer.append(TITLE_SEPARATOR);
        }
        buffer.append(step);
        buffer.append('/');
        buffer.append(totalSteps);
        buffer.append(')');
        return buffer.toString();
    }

    private String computeBufferingStatusCanceledText() {
        StringBuilder buffer = new StringBuilder(OPEN_PARENTHESIS);
        if (selectionBean == null) {
            buffer.append(BUFFERING_STATUS_CANCELED);
            buffer.append(SPACE);
        } else {
            buffer.append(selectionBean.getMessage(BUFFERING_STATUS_CANCELED));
            buffer.append(SPACE);
        }
        buffer.append(step);
        buffer.append('/');
        buffer.append(totalSteps);
        buffer.append(')');
        return buffer.toString();
    }

    private String computeDomainsLabelText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(DOMAINS);
            buffer.append(TITLE_SEPARATOR);
        } else {
            buffer.append(selectionBean.getMessage(DOMAINS));
            buffer.append(TITLE_SEPARATOR);
        }
        return buffer.toString();
    }

    private String computeDomainLabelText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(DOMAIN);
            buffer.append(TITLE_SEPARATOR);
        } else {
            buffer.append(selectionBean.getMessage(DOMAIN));
            buffer.append(TITLE_SEPARATOR);
        }
        return buffer.toString();
    }

    private String computeDeviceClassLabelText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(DEVICE_CLASS);
            buffer.append(TITLE_SEPARATOR);
        } else {
            buffer.append(selectionBean.getMessage(DEVICE_CLASS));
            buffer.append(TITLE_SEPARATOR);
        }
        return buffer.toString();
    }

    private String computeAttributeLabelText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(ATTRIBUTE);
            buffer.append(TITLE_SEPARATOR);
        } else {
            buffer.append(selectionBean.getMessage(ATTRIBUTE));
            buffer.append(TITLE_SEPARATOR);
        }
        return buffer.toString();
    }

    private String computeOkButtonText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(ATTRIBUTE_ADD);
        } else {
            buffer.append(selectionBean.getMessage(ATTRIBUTE_ADD));
        }
        return buffer.toString();
    }

    private String computeDomainsButtonText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(DOMAINS_REG_EXP);
        } else {
            buffer.append(selectionBean.getMessage(DOMAINS_REG_EXP));
        }
        return buffer.toString();
    }

    private String computeSelectionLabelText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(SELECTION);
            buffer.append(TITLE_SEPARATOR);
        } else {
            buffer.append(selectionBean.getMessage(SELECTION));
            buffer.append(TITLE_SEPARATOR);
        }
        return buffer.toString();
    }

    private String computeSelectReverseButtonText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(SELECTION_REVERSE_TEXT);
        } else {
            buffer.append(selectionBean.getMessage(SELECTION_REVERSE_TEXT));
        }
        return buffer.toString();
    }

    private String computeSelectReverseButtonTooltipText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(SELECTION_REVERSE_TOOLTIP_TEXT);
        } else {
            buffer.append(selectionBean.getMessage(SELECTION_REVERSE_TOOLTIP_TEXT));
        }
        return buffer.toString();
    }

    private String computeSelectNoneButtonText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(SELECTION_NONE_TEXT);
        } else {
            buffer.append(selectionBean.getMessage(SELECTION_NONE_TEXT));
        }
        return buffer.toString();
    }

    private String computeSelectNoneButtonTooltipText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(SELECTION_NONE_TOOLTIP_TEXT);
        } else {
            buffer.append(selectionBean.getMessage(SELECTION_NONE_TOOLTIP_TEXT));
        }
        return buffer.toString();
    }

    private String computeSelectAllButtonText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(SELECTION_ALL_TEXT);
        } else {
            buffer.append(selectionBean.getMessage(SELECTION_ALL_TEXT));
        }
        return buffer.toString();
    }

    private String computeSelectAllButtonTooltipText() {
        StringBuilder buffer = new StringBuilder();
        if (selectionBean == null) {
            buffer.append(SELECTION_ALL_TOOLTIP_TEXT);
        } else {
            buffer.append(selectionBean.getMessage(SELECTION_ALL_TOOLTIP_TEXT));
        }
        return buffer.toString();
    }

    /**
     * @return Returns the attributeComboBox.
     */
    public AttributesSelectComboBox getAttributeComboBox() {
        return attributeComboBox;
    }

    /**
     * @return Returns the deviceClassComboBox.
     */
    public AttributesSelectComboBox getDeviceClassComboBox() {
        return deviceClassComboBox;
    }

    /**
     * @return Returns the domainComboBox.
     */
    public AttributesSelectComboBox getDomainComboBox() {
        return domainComboBox;
    }

    public String getDomainsRegExp() {
        return this.domainsField.getText();
    }

    public AttributesSelectTable getAttributesSelectTable() {
        return attributesSelectTable;
    }

    public JPanel getTopHalf() {
        return topHalf;
    }

    @Override
    public synchronized void stepDone(int step, int totalSteps) {
        this.step = step;
        this.totalSteps = totalSteps;
        if (bufferingStatusLabel != null) {
            bufferingStatusLabel.setText(computeBufferingStatusText());
            if (step == totalSteps) {
                bufferingStatusLabel.setForeground(Color.GREEN.darker());
            } else {
                bufferingStatusLabel.setForeground(Color.RED);
            }
            bufferingStatusLabel.revalidate();
            if (topHalf != null) {
                topHalf.revalidate();
            }
        }
    }

    @Override
    public synchronized void allDone() {
        if (step != totalSteps) {
            if (bufferingStatusLabel != null) {
                bufferingStatusLabel.setText(computeBufferingStatusCanceledText());
            }
        }
    }

    public synchronized void refreshTexts() {
        if (domainsLabel != null) {
            domainsLabel.setText(computeDomainsLabelText());
        }
        if (domainLabel != null) {
            domainLabel.setText(computeDomainLabelText());
        }
        if (deviceClassLabel != null) {
            deviceClassLabel.setText(computeDeviceClassLabelText());
        }
        if (attributeLabel != null) {
            attributeLabel.setText(computeAttributeLabelText());
        }
        if (okButton != null) {
            okButton.setText(computeOkButtonText());
        }
        if (domainsButton != null) {
            domainsButton.setText(computeDomainsButtonText());
            domainsButton.setToolTipText(domainsButton.getText());
        }
        if (selectionLabel != null) {
            selectionLabel.setText(computeSelectionLabelText());
        }
        if (selectReverseButton != null) {
            selectReverseButton.setText(computeSelectReverseButtonText());
            selectReverseButton.setToolTipText(computeSelectReverseButtonTooltipText());
        }
        if (selectNoneButton != null) {
            selectNoneButton.setText(computeSelectNoneButtonText());
            selectNoneButton.setText(computeSelectNoneButtonTooltipText());
        }
        if (selectAllButton != null) {
            selectAllButton.setText(computeSelectAllButtonText());
            selectAllButton.setText(computeSelectAllButtonTooltipText());
        }
        if ((attributesSelectTable != null) && (attributesSelectTable.getModel() != null)) {
            attributesSelectTable.getModel().fireTableStructureChanged();
        }
        stepDone(step, totalSteps);
        selectionPanel.revalidate();
    }

    public synchronized void clean() {
        removeAll();
        if (selectionPanel != null) {
            selectionPanel.removeAll();
        }
        if (topHalf != null) {
            topHalf.removeAll();
        }
        if (attributesSelectTable != null) {
            attributesSelectTable.setModel(null);
        }
        attributesSelectTable = null;
        domainsLabel = null;
        domainLabel = null;
        deviceClassLabel = null;
        attributeLabel = null;
        okButton = null;
        domainsButton = null;
        selectionLabel = null;
        selectReverseButton = null;
        selectNoneButton = null;
        selectAllButton = null;
        domainsField = null;
        if (domainComboBox != null) {
            domainComboBox.removeAllItems();
        }
        domainComboBox = null;
        if (deviceClassComboBox != null) {
            deviceClassComboBox.removeAllItems();
        }
        deviceClassComboBox = null;
        if (attributeComboBox != null) {
            attributeComboBox.removeAllItems();
        }
        attributeComboBox = null;
        bufferingStatusLabel = null;
        selectionPanel = null;
        topHalf = null;
    }

    public synchronized void setGlobalBackground(Color bg) {
        setBackground(bg);
        if (topHalf != null) {
            topHalf.setBackground(bg);
        }
        if (selectionPanel != null) {
            selectionPanel.setBackground(bg);
        }
        if (scrollpane != null) {
            scrollpane.setBackground(bg);
            scrollpane.getViewport().setBackground(bg);
            scrollpane.getHorizontalScrollBar().setBackground(bg);
            scrollpane.getVerticalScrollBar().setBackground(bg);
        }
        if (selectReverseButton != null) {
            selectReverseButton.setBackground(bg);
        }
        if (selectNoneButton != null) {
            selectNoneButton.setBackground(bg);
        }
        if (selectAllButton != null) {
            selectAllButton.setBackground(bg);
        }
        if (okButton != null) {
            okButton.setBackground(bg);
        }
        if (domainsButton != null) {
            domainsButton.setBackground(bg);
        }
    }

    public synchronized void setEditionEnabled(boolean enabled) {
        if (okButton != null) {
            okButton.setEnabled(enabled);
        }
        if (domainsButton != null) {
            domainsButton.setEnabled(enabled);
        }
        if (selectReverseButton != null) {
            selectReverseButton.setEnabled(enabled);
        }
        if (selectNoneButton != null) {
            selectNoneButton.setEnabled(enabled);
        }
        if (selectAllButton != null) {
            selectAllButton.setEnabled(enabled);
        }
        if (domainComboBox != null) {
            domainComboBox.setEnabled(enabled);
            domainComboBox.setEditable(enabled);
        }
        if (deviceClassComboBox != null) {
            deviceClassComboBox.setEnabled(enabled);
            deviceClassComboBox.setEditable(enabled);
        }
        if (attributeComboBox != null) {
            attributeComboBox.setEnabled(enabled);
            attributeComboBox.setEditable(enabled);
        }
        if (domainsField != null) {
            domainsField.setEnabled(enabled);
        }
        if (attributesSelectTable != null) {
            attributesSelectTable.setEnabled(enabled);
        }
    }

}
