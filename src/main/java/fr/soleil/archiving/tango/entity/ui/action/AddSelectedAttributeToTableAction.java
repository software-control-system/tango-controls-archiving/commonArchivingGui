package fr.soleil.archiving.tango.entity.ui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.archiving.tango.entity.Attribute;
import fr.soleil.archiving.tango.entity.DeviceClass;
import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.datasource.ITangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.model.AttributesSelectTableModel;
import fr.soleil.archiving.tango.entity.ui.AttributesSelectComboBox;

public class AddSelectedAttributeToTableAction extends AbstractAction {

    private static final long serialVersionUID = -1541755153615015922L;

    private final AttributeTableSelectionBean selectionBean;

    /**
     * @param name
     */
    public AddSelectedAttributeToTableAction(String name, AttributeTableSelectionBean selectionBean) {
        super();
        this.putValue(Action.NAME, name);
        this.selectionBean = selectionBean;
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        if ((selectionBean != null) && (selectionBean.getSelectionPanel()) != null
                && (selectionBean.getTangoManager() != null)) {
            AttributesSelectComboBox domainComboBox = selectionBean.getSelectionPanel().getDomainComboBox();
            AttributesSelectComboBox deviceClassComboBox = selectionBean.getSelectionPanel().getDeviceClassComboBox();
            AttributesSelectComboBox attributesSelectComboBox = selectionBean.getSelectionPanel()
                    .getAttributeComboBox();

            Domain selectedDomain = domainComboBox.getSelectedDomain();
            DeviceClass selectedDeviceClass = deviceClassComboBox.getSelectedDeviceClass();
            if (selectedDeviceClass != null) {
                Attribute selectedAttribute = attributesSelectComboBox.getSelectedAttribute();
                if (selectedAttribute != null) {
                    ITangoEntitiesSelectionManager manager = selectionBean.getTangoManager();
                    Attribute[] selectedAttributes = manager.loadAttributes(selectedDeviceClass, selectedAttribute);

                    AttributesSelectTableModel attributesSelectTableModel = selectionBean.getSelectionPanel()
                            .getAttributesSelectTable().getModel();
                    for (int i = 0; i < selectedAttributes.length; i++) {
                        Attribute nextAttr = selectedAttributes[i];
                        String nextAttrDomainName = nextAttr.getDomainName();
                        if (selectedDomain == null || selectedDomain.getName().equals(nextAttrDomainName)) {
                            attributesSelectTableModel.addAttribute(nextAttr);
                        }
                        nextAttrDomainName = null;
                        nextAttr = null;
                    }
                    attributesSelectTableModel = null;
                    selectedAttributes = null;
                }
                selectedAttribute = null;
            }
            selectedDeviceClass = null;
            selectedDomain = null;
            attributesSelectComboBox = null;
            deviceClassComboBox = null;
            domainComboBox = null;
        }
    }
}
