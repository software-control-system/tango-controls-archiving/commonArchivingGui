package fr.soleil.archiving.tango.entity.ui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.soleil.archiving.tango.entity.Domain;
import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.datasource.ITangoEntitiesSelectionManager;
import fr.soleil.archiving.tango.entity.ui.AttributesSelectComboBox;
import fr.soleil.lib.project.ObjectUtils;

public class DomainsFilterinComboBoxAction extends AbstractAction {

    private static final long serialVersionUID = -3976624034802488533L;

    private final AttributeTableSelectionBean selectionBean;
    private final Logger logger = LoggerFactory.getLogger(DomainsFilterinComboBoxAction.class);

    public DomainsFilterinComboBoxAction(final String name, final AttributeTableSelectionBean selectionBean) {
        super();
        putValue(Action.NAME, name);
        putValue(Action.SHORT_DESCRIPTION, name);
        this.selectionBean = selectionBean;
    }

    @Override
    public void actionPerformed(final ActionEvent evt) {
        if (selectionBean != null && selectionBean.getSelectionPanel() != null
                && selectionBean.getTangoManager() != null) {
            final ITangoEntitiesSelectionManager manager = selectionBean.getTangoManager();
            final String domainsRegExp = selectionBean.getSelectionPanel().getDomainsRegExp();
            final AttributesSelectComboBox attributeComboBox = selectionBean.getSelectionPanel().getAttributeComboBox();
            final AttributesSelectComboBox deviceClassComboBox = selectionBean.getSelectionPanel()
                    .getDeviceClassComboBox();
            final AttributesSelectComboBox domainComboBox = selectionBean.getSelectionPanel().getDomainComboBox();

            attributeComboBox.reset();
            domainComboBox.selectNeutralElement();

            Domain[] domains = null;
            try {
                domains = manager.loadDomains(domainsRegExp);
                deviceClassComboBox.setDeviceClasses(domains);
                if (selectionBean != null) {
                    logger.debug(selectionBean
                            .getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.Domains.Filter.OK"));
                }
            } catch (final Exception e) {
                e.printStackTrace();
                logger.error(ObjectUtils.EMPTY_STRING, e);
                if (selectionBean != null) {
                    logger.debug(selectionBean
                            .getMessage("fr.soleil.archiving.tango.entity.AttributesSelection.Domains.Filter.KO"));
                }
            }
        }
    }
}
