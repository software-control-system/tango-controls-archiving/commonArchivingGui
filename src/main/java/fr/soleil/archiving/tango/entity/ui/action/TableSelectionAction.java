package fr.soleil.archiving.tango.entity.ui.action;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;

import fr.soleil.archiving.tango.entity.bean.AttributeTableSelectionBean;
import fr.soleil.archiving.tango.entity.ui.AttributesSelectTable;

public class TableSelectionAction extends AbstractAction {

    private static final long serialVersionUID = -2344729514559517720L;

    private final int type;
    private final AttributeTableSelectionBean selectionBean;

    public static final int SELECT_REVERSE_TYPE = 0;
    public static final int SELECT_ALL_TYPE = 1;
    public static final int SELECT_NONE_TYPE = 2;

    /**
     * @param name
     */
    public TableSelectionAction(String name, int type, AttributeTableSelectionBean selectionBean) {
        super();
        if ((type != SELECT_REVERSE_TYPE) && (type != SELECT_ALL_TYPE) && (type != SELECT_NONE_TYPE)) {
            throw new IllegalArgumentException("Expected either of " + SELECT_REVERSE_TYPE + "," + SELECT_ALL_TYPE
                    + "," + SELECT_NONE_TYPE + " as a parameter. Received " + type + " instead.");
        }
        this.putValue(Action.NAME, name);
        this.type = type;
        this.selectionBean = selectionBean;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        if (selectionBean != null && selectionBean.getSelectionPanel() != null) {
            AttributesSelectTable table = selectionBean.getSelectionPanel().getAttributesSelectTable();
            table.applyChange();
            switch (this.type) {
                case SELECT_REVERSE_TYPE:
                    table.reverseSelection();
                    break;
                case SELECT_ALL_TYPE:
                    table.selectAllOrNone(true);
                    break;
                case SELECT_NONE_TYPE:
                    table.selectAllOrNone(false);
                    break;
            }
            table = null;
        }
    }

}
