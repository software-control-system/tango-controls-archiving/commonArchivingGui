package fr.soleil.archiving.tango.entity.ui.comparator;

import java.text.Collator;
import java.util.Comparator;

import fr.soleil.archiving.tango.entity.Attribute;

/**
 * Implements a Comparator on Attribute objects, useful to sort the Attribute
 * lists. The comparison can be on any one of the Attribute fields, depending on
 * the type specified on constructing the comparator.
 */
public class AttributeComparator implements Comparator<Attribute> {

    public static final int COMPARE_DEVICE_CLASS = 0;
    public static final int COMPARE_DEVICE = 1;
    public static final int COMPARE_NAME = 2;

    /**
     * The ContextData list isn't sorted relative to <code>fieldToCompare</code> yet
     */
    public static final int NO_SORT = 0;
    /**
     * The ContextData list is sorted by ascending <code>fieldToCompare</code>.
     */
    public static final int SORT_UP = 1;
    /**
     * The ContextData list is sorted by descending <code>fieldToCompare</code>.
     */
    public static final int SORT_DOWN = 2;

    private final int fieldToCompare;

    /**
     * Builds a comparator on the desired ContextData field
     * 
     * @param _fieldToCompare
     *            The field on which the comparison will be done
     * @throws IllegalArgumentException
     *             If _fieldToCompare isn't in (COMPARE_ID, COMPARE_TIME
     *             COMPARE_NAME, COMPARE_AUTHOR, COMPARE_REASON,
     *             COMPARE_DESCRIPTION)
     */
    public AttributeComparator(int _fieldToCompare) throws IllegalArgumentException {
        super();
        this.fieldToCompare = _fieldToCompare;

        if (_fieldToCompare != COMPARE_DEVICE && _fieldToCompare != COMPARE_DEVICE_CLASS
                && _fieldToCompare != COMPARE_NAME) {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the new sort type to switch to on user request, given the current
     * one.
     * 
     * @param formerSortType
     *            The current sort type
     * @return The new sort type
     */
    public static int getNewSortType(int formerSortType) {
        int ret;

        if (formerSortType == AttributeComparator.NO_SORT || formerSortType == AttributeComparator.SORT_DOWN) {
            ret = AttributeComparator.SORT_UP;
        } else {
            ret = AttributeComparator.SORT_DOWN;
        }

        return ret;
    }

    @Override
    public int compare(Attribute attr1, Attribute attr2) {

        if (attr1 == null && attr2 == null) {
            return 0;
        } else if (attr1 == null) {
            return -1;
        } else if (attr2 == null) {
            return 1;
        } else {
            switch (fieldToCompare) {
                case COMPARE_DEVICE:
                    return Collator.getInstance().compare(attr1.getDeviceName(), attr2.getDeviceName());
                case COMPARE_DEVICE_CLASS:
                    return Collator.getInstance().compare(attr1.getDeviceClass(), attr2.getDeviceClass());
                case COMPARE_NAME:
                    return Collator.getInstance().compare(attr1.getName(), attr2.getName());
                default:
                    throw new IllegalArgumentException();
            }
        }
    }

}
