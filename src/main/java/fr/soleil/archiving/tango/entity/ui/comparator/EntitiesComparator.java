package fr.soleil.archiving.tango.entity.ui.comparator;

import java.text.Collator;
import java.util.Comparator;

import fr.soleil.archiving.tango.entity.Entity;

public class EntitiesComparator implements Comparator<Entity> {

    public EntitiesComparator() {
        super();
    }

    @Override
    public int compare(Entity e1, Entity e2) {
        if (e1 == null && e2 == null) {
            return 0;
        } else if (e1 == null) {
            return -1;
        } else if (e2 == null) {
            return 1;
        } else {
            return Collator.getInstance().compare(e1.getName(), e2.getName());
        }
    }

}
