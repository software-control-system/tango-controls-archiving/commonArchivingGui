package fr.soleil.archiving.tango.log;

import java.io.IOException;

/**
 * Defines everything that has to with logging in an application. Logs are
 * divided in two categories:
 * <ul>
 * <li>Messages that are logged into the application diary for future debugging and control</li>
 * <li>Confirmation and error messages displayed to the user upon the realization of an action</li>
 * </ul>
 * All messages have a criticality level; the most critic messages have the
 * lowest criticality index. A message will be logged if and only if its
 * criticality is under the criticality threshold of the logger.
 * 
 * @author CLAISSE
 */
public interface ILogger {

    /**
     * Log only critic messages
     */
    public static final int LEVEL_CRITIC = 1;
    /**
     * Log critic and error messages
     */
    public static final int LEVEL_ERROR = 3;
    /**
     * Log critic, error, and warning messages
     */
    public static final int LEVEL_WARNING = 5;
    /**
     * Log critic, error, warning, and info messages
     */
    public static final int LEVEL_INFO = 7;
    /**
     * Log all messages
     */
    public static final int LEVEL_DEBUG = 9;

    /**
     * Prefix for critic messages
     */
    public static final String CRITIC = "CRITIC";
    /**
     * Prefix for error messages
     */
    public static final String ERROR = "ERROR";
    /**
     * Prefix for warning messages
     */
    public static final String WARNING = "WARNING";
    /**
     * Prefix for info messages
     */
    public static final String INFO = "INFO";
    /**
     * Prefix for debug messages
     */
    public static final String DEBUG = "DEBUG";

    /**
     * Returns the current criticality threshold, under which a message is
     * logged.
     * 
     * @return The current criticality threshold, under which a message is
     *         logged
     */
    public int getTraceLevel();

    /**
     * Sets the new criticality threshold, under which a message will be logged
     * 
     * @param level
     *            The new criticality threshold, under which a message will be
     *            logged
     */
    public void setTraceLevel(int level);

    /**
     * Logs a message, if its criticality is under the current criticality
     * threshold; otherwise does nothing.
     * 
     * @param level
     *            The message's criticality
     * @param o
     *            The Object to log (can be Exception, String, ...)
     */
    public void trace(int level, Object o);

    /**
     * Initializes the resource used for the daily application diary.
     * 
     * @param path
     *            The path where the diary will be created
     * @throws IOException
     */
    public void initDiaryWriter(String path) throws IOException;

    /**
     * Closes the resource used for the daily application diary.
     */
    public void close();

    /**
     * Returns the criticality level associated with a given log prefix.
     * 
     * @param level_s
     *            The log prefix
     * @return The criticality level associated with a given log prefix
     */
    public int getTraceLevel(String level_s);
}
